# Proyecto Final curso Full Stack - Rolling Code School

Prueba el proyecto Online [https://smartechstore.vercel.app](https://smartechstore.vercel.app)

Para probarlo en Local

  - Clonar este 
  ```sh
    cd e-commerce-tecnologia-cliente-3d-rram
    npm install
    npm start
 ```
También:
  - Clonar este repositorio [https://gitlab.com/rickywep/e-commerce-tecnologia-servidor-3d-rram](https://gitlab.com/rickywep/e-commerce-tecnologia-servidor-3d-rram)
```sh
   cd e-commerce-tecnologia-servidor-3d-rram
   npm install
   npm run dev
```
para probar las funciones del admin
user: admin@gmail.com password: admin1
