import React, { useEffect, useState, useContext } from "react";
import clienteAxios from "../../config/axios";
import { Container, Table, Button } from "react-bootstrap";
import UsuarioAdmin from "../../components/UsuarioAdmin";
import ModalCrearAdmin from "../../components/ModalCrearAdmin";
import AuthContext from "../../context/auth/authContext";


const UsuariosAdmin = () => {


  const authContext = useContext(AuthContext);
  const { usuario } = authContext;

  

  const [show, setShow] = useState(false);
  const handleShow = () => setShow(true);

  const [usuarios, setUsuarios] = useState([]);

  useEffect(() => {
    const listarUsuarios = async () => {
      const respuesta = await clienteAxios.get("/api/usuarios");

      setUsuarios(respuesta.data);
    };
    listarUsuarios();
  }, [show]);

  return (
    <Container>
      {usuario &&
        <>
          <div className="d-flex flex-row-reverse my-3">
            <Button onClick={handleShow}>crear usuario admin</Button>
          </div>
          <ModalCrearAdmin
            show={show}
            setShow={setShow}
          />
          <h4>Administradores</h4>
          <Table responsive bordered>
            <thead style={{ backgroundColor: "var(--details)", color: "white" }}>
              <tr>
                <th>Nombre</th>
                <th>Email</th>
              </tr>
            </thead>
            <tbody>
              {usuarios.map((usuario) => (
                usuario.is_admin ?
                  (<UsuarioAdmin key={usuario._id} usuario={usuario} />)
                  : null

              ))}
            </tbody>
          </Table>
          <h4>Usuarios</h4>
          <Table responsive bordered>
            <thead style={{ backgroundColor: "var(--details)", color: "white" }}>
              <tr>
                <th>Nombre</th>
                <th>Email</th>
              </tr>
            </thead>
            <tbody>
              {usuarios.map((usuario) => (
                !usuario.is_admin ?
                  (<UsuarioAdmin key={usuario._id} usuario={usuario} />)
                  : null

              ))}
            </tbody>
          </Table>
        </>
      }
    </Container>
  );
};

export default UsuariosAdmin;
