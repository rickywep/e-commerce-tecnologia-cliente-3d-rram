import React, { useEffect, useState, useContext } from "react";
import clienteAxios from "../../config/axios";
import TurnoAdmin from '../../components/TurnoAdmin';
import { Table, Container, } from "react-bootstrap";
import moment from 'moment'
import TablaHoyAdmin from "../../components/TablaHoyAdmin";
import AuthContext from "../../context/auth/authContext";


const TurnosAdmin = () => {
  const [turnos, setTurnos] = useState([]);
  const authContext = useContext(AuthContext);
  const { usuario } = authContext;

  useEffect(() => {
    const listarTurnos = async () => {
      const respuesta = await clienteAxios.get("/api/turnos");
      const sortedTurnos = respuesta.data.sort((a, b) => new Date(a.diaYHora) - new Date(b.diaYHora))

      setTurnos(sortedTurnos);
    };
    listarTurnos();
  }, []);

  return (
    <Container className="my-5">
      {usuario &&
        <>
          <h3>Turnos de Hoy</h3>
          <Table responsive bordered className="my-2">
            <thead style={{ backgroundColor: "var(--details)", color: "white" }}>
              <tr>
                <th>Dia y Hora</th>
                <th>Nombre</th>
                <th>Email</th>
                <th>Celular</th>
              </tr>
            </thead>
            <tbody>
              {turnos.map((turno) => (
                moment.utc(turno.diaYHora).format('MM/DD/YYYY') === moment().format('MM/DD/YYYY') &&
                <TablaHoyAdmin key={turno._id} turno={turno} />
              ))}
            </tbody>
          </Table>
          <h3 className="mt-4">Proximos turnos</h3>
          <Table responsive bordered className="my-2">
            <thead style={{ backgroundColor: "var(--details)", color: "white" }}>
              <tr>
                <th>Dia y Hora</th>
                <th>Nombre</th>
                <th>Email</th>
                <th>Celular</th>
              </tr>
            </thead>
            <tbody>
              {turnos.map((turno) => (
                moment.utc(turno.diaYHora).format('MM/DD/YYYY') > moment().format('MM/DD/YYYY') ?
                  <TurnoAdmin key={turno._id} turno={turno} />
                  : null
              ))}
            </tbody>
          </Table>
        </>}
    </Container>
  );
};

export default TurnosAdmin;
