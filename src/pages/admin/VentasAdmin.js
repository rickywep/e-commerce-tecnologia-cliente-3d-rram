import React, { useState, useEffect, useContext } from 'react';
import clienteAxios from "../../config/axios";
import VentaAdmin from '../../components/VentaAdmin';
import { Container, Table } from 'react-bootstrap';
import AuthContext from "../../context/auth/authContext";

const VentasAdmin = () => {
    const authContext = useContext(AuthContext);
    const { usuario } = authContext;
    const [ventas, setVentas] = useState([]);
    useEffect(() => {
        const listarVentas = async () => {
            const respuesta = await clienteAxios.get("/api/compras");
            setVentas(respuesta.data);
        };
        listarVentas();

    }, []);
    return (
        <Container fluid className="mt-3 p-5">
            {usuario &&
                <>
                    <h2 className="text-center mb-5">Registro de Ventas</h2>
                    <div className="table-responsive">
                        <Table bordered className="m-1" >
                            <thead style={{ backgroundColor: "var(--details)", color: "white" }}>
                                <tr>
                                    <th>Productos</th>
                                    <th>Nombre</th>
                                    <th>Ciudad</th>
                                    <th>Domicilio</th>
                                    <th>Telefono</th>
                                </tr>
                            </thead>
                            <tbody>
                                {ventas.map((venta,i) => (
                                    (<VentaAdmin key={i} venta={venta} />)
                                ))}
                            </tbody>
                        </Table>
                    </div>

                </>
            }
        </Container>
    );
};

export default VentasAdmin;