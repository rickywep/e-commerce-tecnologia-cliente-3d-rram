import React, { useEffect, useState, useContext } from "react";
import clienteAxios from "../../config/axios";
import { Container, Table } from "react-bootstrap";
import MensajeAdmin from "../../components/MensajeAdmin";
import AuthContext from "../../context/auth/authContext";


const MensajesAdmin = () => {
  const authContext = useContext(AuthContext);
  const { usuario } = authContext;
  const [mensajes, setMensajes] = useState([]);

  useEffect(() => {
    const listarMensajes = async () => {
      const respuesta = await clienteAxios.get("/api/mensajes");
      setMensajes(respuesta.data);
    };
    listarMensajes();
  }, []);

  return (
    <Container className="my-5">
      {
        usuario &&
        <>
          <Table responsive bordered>
            <thead style={{ backgroundColor: "var(--details)", color: "white" }}>
              <tr>
                <th><h5 className="text-center m-3">Mensajes</h5></th>
              </tr>
            </thead>
            <tbody>
              {mensajes.map((mensaje) => (
                !mensaje.is_leido ?
                  <MensajeAdmin key={mensaje._id} mensaje={mensaje} />
                  : null
              ))}
            </tbody>
          </Table>
          <Table responsive bordered className="my-5">
            <thead className="bg-secondary" style={{ color: "white" }}>
              <tr>
                <th>mensajes leidos</th>
              </tr>
            </thead>
            <tbody>
              {mensajes.map((mensaje) => (
                mensaje.is_leido ?
                  <MensajeAdmin key={mensaje._id} mensaje={mensaje} />
                  : null
              ))}
            </tbody>
          </Table>
        </>
      }
    </Container>
  );
};

export default MensajesAdmin;
