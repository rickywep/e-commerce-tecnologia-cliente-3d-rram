import React from "react";
import { Container, Row,Col } from "react-bootstrap";

const InicioAdmin = () => {
  return (
    <Container>
      <h1 className="text-center m-3">Bienvenido Admin</h1>
      <Row className="mt-5">
        <Col>
          <h3 style={{color: "var(--details)"}}>Ultimos Usuarios</h3>
        </Col>
        <Col>
          <h3 style={{color: "var(--details)"}}>Ultimos Productos</h3>
        </Col>
        <Col>
          <h3 style={{color: "var(--details)"}}>Ultimos Turnos</h3>
        </Col>
      </Row>
    </Container>
  );
};

export default InicioAdmin;
