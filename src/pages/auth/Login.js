import React, { useState, useContext, useEffect } from "react";
import { Link } from "react-router-dom";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Alert,
} from "react-bootstrap";
import LogoGrande from "../../images/logos/logo_grande.svg";
import AuthContext from "../../context/auth/authContext";
import AlertaContext from "../../context/alertas/alertaContext";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";

export default function Login(props) {
  const alertaContext = useContext(AlertaContext);
  const { alerta, mostrarAlerta } = alertaContext;
  // Obtener el context de Autenticación
  const authContext = useContext(AuthContext);
  const {
    mensaje,
    autenticado,
    iniciarSesion,
    usuario,
    is_admin,
  } = authContext;

  useEffect(() => {
    if (usuario) {
      if (is_admin) {
        props.history.push("/admin/usuarios");
      } else {
        props.history.push("/");
      }
    }

    if (mensaje) {
      mostrarAlerta(mensaje.msg, mensaje.categoria);
    }
    // eslint-disable-next-line
  }, [mensaje, autenticado, usuario, props.history, is_admin]);

  const [user, setUser] = useState({
    email: "",
    password: "",
  });

  // Extraemos del usuario
  const { email, password } = user;
  // state ver password o no
  const [verpass, setVerpass] = useState(false);

  // Cuando cambian los campos
  const onChangeUsuario = (e) => {
    setUser({
      ...user,
      [e.target.name]: e.target.value,
    });
  };
  // Cuando el usuario quiere iniciar sesión
  const onSubmitLogin = (e) => {
    e.preventDefault();
    
    iniciarSesion({ email, password });
  };
  // funcion para ver o no el password
  const verPass = () => {
    const pass = verpass;
    setVerpass(!pass);
  };

  return (
    <Container className="contenedor">
      <Row className="mt-5">
        <Col xs={12} sm={8} md={6} className="mx-auto">
          {alerta ? (
            <Alert variant={alerta.categoria} className="mt-2 text-center">
              {alerta.msg}
            </Alert>
          ) : null}
          <Card bg="light">
            <Card.Header style={{ backgroundColor: "var(--background)" }}>
              <img src={LogoGrande} alt="logo grande" />
            </Card.Header>
            <Card.Body>
              <Form onSubmit={onSubmitLogin}>
                <Form.Group controlId="formLoginEmail">
                  <Row>
                    <Col sm={2}>
                      <Form.Label>Email</Form.Label>
                    </Col>
                    <Col sm={10}>
                      <Form.Control
                        type="email"
                        name="email"
                        required
                        placeholder="Ingrese su email"
                        onChange={onChangeUsuario}
                        value={email}
                        maxLength="30"
                      />
                    </Col>
                  </Row>
                </Form.Group>
                <Form.Group controlId="formLoginPassword">
                  <Row>
                    <Col sm={2}>
                      <Form.Label>Password</Form.Label>
                    </Col>
                    <Col sm={10}>
                      <Form.Control
                        type={!verpass ? "password" : "text"}
                        name="password"
                        required
                        placeholder="Ingrese su password"
                        onChange={onChangeUsuario}
                        value={password}
                        maxLength="30"
                      />
                      <FontAwesomeIcon
                        icon={verpass ? faEye : faEyeSlash}
                        onClick={verPass}
                        size="2x"
                        className="iconLogin p-1"
                        style={{ color: "var(--details)" }}
                      />
                    </Col>
                  </Row>
                </Form.Group>
                <Row>
                  <Button
                    className="mx-auto text-white"
                    variant="dark"
                    type="submit"
                    style={{ backgroundColor: "var(--details)" }}
                  >
                    Iniciar Sesión
                  </Button>
                </Row>
                <Row>
                  <Link
                    className="mx-auto mt-2"
                    to="/registro"
                    style={{ color: "var(--details)" }}
                  >
                    ¿No tiene una cuenta? Cree una aquí{" "}
                  </Link>
                </Row>
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
