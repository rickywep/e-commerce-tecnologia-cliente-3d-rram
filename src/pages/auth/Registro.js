import React, { useState, useContext, useEffect } from "react";
import { Link } from "react-router-dom";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Alert,
} from "react-bootstrap";
import AuthContext from "../../context/auth/authContext";
import AlertaContext from "../../context/alertas/alertaContext";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import LogoGrande from "../../images/logos/logo_grande.svg";

function Registro(props) {
  const alertaContext = useContext(AlertaContext);
  const { alerta, mostrarAlerta } = alertaContext;
  // Obtener el context de Autenticación
  const authContext = useContext(AuthContext);
  const { mensaje, autenticado, registrarUsuario } = authContext;

  useEffect(() => {
    if (autenticado) {
      props.history.push("/");
    }
    if (mensaje) {
      mostrarAlerta(mensaje.msg, mensaje.categoria);
    }
    // eslint-disable-next-line
  }, [mensaje, autenticado, props.history]);

  // creo state de usuario
  const [usuario, setUsuario] = useState({
    nombre: "",
    apellido: "",
    email: "",
    password: "",
    passwordconfirm: "",
  });
  // Extraemos del usuario
  const { nombre, apellido, email, password, passwordconfirm } = usuario;
  // state ver password o no
  const [verpass, setVerpass] = useState(false);
  // state ver password o no
  const [verpassconf, setVerpassconf] = useState(false);

  // Rellenar los campos mientras se tipea
  const onChangeUsuario = (e) => {
    setUsuario({
      ...usuario,
      [e.target.name]: e.target.value,
    });
  };
  // Cuando el usuario quiere registrarse
  const onSubmitRegistro = (e) => {
    e.preventDefault();

    //password de 6 caracteres
    if (password.length < 6) {
      mostrarAlerta("El password debe ser al menos de 6 caracteres", "danger");
      return;
    }

    if (password !== passwordconfirm) {
      mostrarAlerta("Los passwords no son iguales", "danger");
      return;
    }
    registrarUsuario({
      nombre,
      apellido,
      email,
      password,
    });
  };
  // funcion para ver o no el password
  const verPass = () => {
    const pass = verpass;
    setVerpass(!pass);
  };
  const verPassconf = () => {
    const passconf = verpassconf;
    setVerpassconf(!passconf);
  };

  return (
    <Container className="contenedor">
      <Row className="mt-5">
        <Col xs={12} sm={8} className="mx-auto">
          {alerta ? (
            <Alert variant={alerta.categoria} className="mt-2 text-center">
              {alerta.msg}
            </Alert>
          ) : null}
          <Card bg="light">
            <Card.Header style={{ backgroundColor: "var(--background)" }}>
              <img src={LogoGrande} alt="logo grande" />
            </Card.Header>
            <Card.Body>
              <Form onSubmit={onSubmitRegistro}>
                <Form.Group controlId="formRegistroNombre">
                  <Row>
                    <Col sm={3}>
                      <Form.Label>Nombre</Form.Label>
                    </Col>
                    <Col sm={9}>
                      <Form.Control
                        type="text"
                        name="nombre"
                        required
                        placeholder="Ingrese su nombre"
                        onChange={onChangeUsuario}
                        value={nombre}
                        maxLength="20"
                      />
                    </Col>
                  </Row>
                </Form.Group>
                <Form.Group controlId="formRegistroApellido">
                  <Row>
                    <Col sm={3}>
                      <Form.Label>Apellido</Form.Label>
                    </Col>
                    <Col sm={9}>
                      <Form.Control
                        type="text"
                        name="apellido"
                        required
                        placeholder="Ingrese su apellido"
                        onChange={onChangeUsuario}
                        value={apellido}
                        maxLength="20"
                      />
                    </Col>
                  </Row>
                </Form.Group>
                <Form.Group controlId="formRegistroEmail">
                  <Row>
                    <Col sm={3}>
                      <Form.Label>Email</Form.Label>
                    </Col>
                    <Col sm={9}>
                      <Form.Control
                        type="email"
                        name="email"
                        required
                        placeholder="Ingrese su email"
                        onChange={onChangeUsuario}
                        value={email}
                        maxLength="30"
                      />
                    </Col>
                  </Row>
                </Form.Group>
                <Form.Group controlId="formRegistroPassword">
                  <Row>
                    <Col sm={3}>
                      <Form.Label>Password</Form.Label>
                    </Col>
                    <Col sm={9}>
                      <Form.Control
                        type={!verpass ? "password" : "text"}
                        name="password"
                        required
                        placeholder="Ingrese su password"
                        onChange={onChangeUsuario}
                        value={password}
                        maxLength="20"
                      />
                      <FontAwesomeIcon
                        icon={verpass ? faEye : faEyeSlash}
                        onClick={verPass}
                        size="2x"
                        className="iconLogin p-1"
                        style={{ color: "var(--details)" }}
                        maxLength="20"
                      />
                    </Col>
                  </Row>
                </Form.Group>
                <Form.Group controlId="formRegistroPasswordconfirm">
                  <Row>
                    <Col sm={3}>
                      <Form.Label>Confirmar Password</Form.Label>
                    </Col>
                    <Col sm={9}>
                      <Form.Control
                        type={!verpassconf ? "password" : "text"}
                        name="passwordconfirm"
                        required
                        placeholder="Ingrese su password de nuevo"
                        onChange={onChangeUsuario}
                        value={passwordconfirm}
                      />
                      <FontAwesomeIcon
                        icon={verpassconf ? faEye : faEyeSlash}
                        onClick={verPassconf}
                        size="2x"
                        className="iconLogin p-1"
                        style={{ color: "var(--details)" }}
                      />
                    </Col>
                  </Row>
                </Form.Group>
                <Row>
                  <Button
                    className="mx-auto text-white"
                    variant="dark"
                    type="submit"
                    style={{ backgroundColor: "var(--details)" }}
                  >
                    Registrarme
                  </Button>
                </Row>
                <Row>
                  <Link
                    className="mx-auto mt-2"
                    to="/login"
                    style={{ color: "var(--details)" }}
                  >
                    ¿Ya tiene una cuenta?{" "}
                    Iniciar sesión
                    {/* <Link to="/login"></Link> */}
                  </Link>
                </Row>
                <Row></Row>
                <Col></Col>
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

export default Registro;
