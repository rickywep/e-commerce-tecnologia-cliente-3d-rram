import React, { Fragment } from "react";
import Carrusel from "../../components/Carrusel";
import { Container, Row, Col } from "react-bootstrap";
import ServiciosHome from "../../components/ServiciosHome";
import Contact from "../../components/Contact";
import Consegui from "../../images/consegui.png"
import Conta from "../../images/conta.png"

const Home = () => {

  return (
    <Fragment>
      <Container fluid className="" style={{ backgroundImage: `url(${Consegui})`, backgroundRepeat: 'no-repeat' }} id="home">
        <Container>
            <Row className="mt-5">
              <Col md={5} className="text-center m-auto" >
                <h1>Consegui tu smartphone</h1>
                <p>
                  Todos los celulares que buscas, sus accesorios y el servicio
                  técnico de mejor calidad en un mismo lugar
              </p>
              </Col>
              <Col className="p-5">
                <Carrusel />
              </Col>
            </Row>
        </Container>
        <hr className="my-2" />
        <Container className="bg-white mb-5 py-5 img-fluid">
            <ServiciosHome />
        </Container>
        <hr className="my-2" />
        <Container className="mt-2 py-2" style={{ backgroundImage: `url(${Conta})`, backgroundRepeat: 'no-repeat' }}>
            <Contact />
        </Container>
      </Container>
    </Fragment>
  );
};

export default Home;
