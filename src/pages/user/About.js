import React, { Fragment } from "react";
import Ejemplo from "../../images/Ejemplo.jpg";
import Contact from "../../components/Contact";
// eslint-disable-next-line
import Jump from 'react-reveal/Jump' 
import { Container } from "react-bootstrap";


const About = () => {
  return (
    <Fragment>
      <Container id="about">
        <div className="my-5">
          <Jump>
            <h1 className="text-center" style={{ color: "var(--details)" }}>
              Una calida bienvenida!
        </h1>
          </Jump>
          <p className="lead m-4">
            Somos una empresa dedicada a la venta de equipos celulares de gama
            alta, nuestro objetivo es ofrecer equipos libres para que no tengan
            que firmar contratos ni adquirir planes que a la larga les costaria
            mucho dinero, a nuestros equipos los podra usar con cualquier operador
            y en cualquier pais sin restricciones de ningun tipo. Somos una
            empresa solida con años de experiencia en este rubro y una larga lista
            de clientes que dan fe de nuestra seriedad y resposabilidad. Con
            nosotros estara mas que seguro.
        </p>
        </div>
      </Container>
      <img className="d-block w-100" src={Ejemplo} alt="ejemplo" />
      <Container>
        <Contact />
      </Container>
    </Fragment>
  );
};

export default About;
