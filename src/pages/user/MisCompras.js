import React, { useState, useEffect, useContext } from 'react'
import { Container } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import LogoChico from "../../images/logos/logo_chico.svg";
import clienteAxios from '../../config/axios';
import AuthContext from "../../context/auth/authContext";
import { ComprasUsuario } from '../../components/ComprasUsuario';
import ReactSpiner from '../../components/ReactSpiner';

export const MisCompras = () => {

  const authContext = useContext(AuthContext);
  const { usuario } = authContext;
  const [compras, setCompras] = useState('')
  const [cargando, setCargando] = useState(true)

  useEffect(() => {
    let id
    if (usuario) {
      id = usuario._id
      const listarCompraUsuario = async () => {
        const respuesta = await clienteAxios.get(`/api/compras/${id}`);
        setCompras(respuesta.data.compras);
        setCargando(false)
      };
      listarCompraUsuario();
    }
  }, [usuario])

  return (
    <Container className="contenedor">
      {
        cargando ? <ReactSpiner />
          :
          <div>
            <h3 className="text-center py-5" style={{color:'var(--details)'}}>Historial de Compras</h3>
            {compras.length !== 0 ?
              <ComprasUsuario compras={compras} />
              :
              <div className="contenedor">
                <div className="row justify-content-center align-items-center minh-100">
                  <div className="col-lg-12">
                    <div>
                      <h1 className="text-center m-5">No tienes compras aún</h1>
                      <img
                        className="img-fluid rounded mx-auto d-block"
                        src={LogoChico}
                        alt="logo"
                        width="100"
                      />
                    </div>
                    <div>
                      <Link className="nav-link" to="/tienda">
                        <p
                          style={{ fontSize: "24px", color: "var(--details)" }}
                          className="text-center"
                        >
                          Visita nuestra tienda
              </p>
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            }
          </div>
      }
    </Container>
  )
}
