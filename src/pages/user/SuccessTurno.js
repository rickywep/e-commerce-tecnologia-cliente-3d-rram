import React from "react";
import LogoChico from "../../images/logos/logo_chico.svg";
import moment from 'moment'


const SuccessTurno = ({ datos }) => {
  let proposedDate = moment.utc(datos.diaYHora).format('MM/DD/YYYY hh:mm:ss A')
  let date = moment(new Date(proposedDate)).subtract(3, 'hours').format('MM/DD/YYYY hh:mm:ss A');
  return (
    <div className="contenedor">
      <div className="row justify-content-center align-items-center minh-100">
        <div className="col-lg-12">
          <div>
            <h1 className="text-center m-5">Tu turno se registro correctamente</h1>
            <img
              className="img-fluid rounded mx-auto d-block"
              src={LogoChico}
              alt="logo"
              width="100"
            />
            <h2 className="text-center m-5">
              Gracias por confiar en nosotros!
            </h2>
          </div>
          <div className="text-center">
            <h3 style={{color: 'var(--details)'}}>Datos de tu turno</h3>
          </div>
          <div className="text-center">
            <p>Nombre: {datos.nombre}</p>
            <p>Email: {datos.email}</p>
            <p>Dia y Hora: {date}</p>

          </div>
        </div>
      </div>
    </div>
  );
};

export default SuccessTurno;
