import React from "react";
import { Link } from "react-router-dom";
import LogoChico from "../../images/logos/logo_chico.svg";

const SuccessCompra = () => {
  return (
    <div className="contenedor">
      <div className="row justify-content-center align-items-center minh-100">
        <div className="col-lg-12">
          <div>
            <h1 className="text-center m-5">Tu compra se realizo con exito</h1>
            <img
              className="img-fluid rounded mx-auto d-block"
              src={LogoChico}
              alt="logo"
              width="100"
            />
            <h2 className="text-center m-5">
              Gracias por confiar en nosotros!
            </h2>
          </div>
          <div>
            <p className="text-center mt-3" style={{ color: "var(--details)" }}>
              ¿Queres Seguir Comprando?
            </p>
            <Link className="nav-link" to="/tienda">
              <p
                style={{ fontSize: "24px", color: "var(--details)" }}
                className="text-center"
              >
                Visita nuestra tienda
              </p>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SuccessCompra;
