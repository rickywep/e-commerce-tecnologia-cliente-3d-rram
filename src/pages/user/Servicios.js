import React, { Fragment } from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
import ImageRepair from "../../images/Servicios/repair.svg";
import ImageDelivery from "../../images/Servicios/delivery.svg";
import ImageMedida from "../../images/Servicios/medida.svg";
import { Link } from "react-router-dom";
import Fade from 'react-reveal/Fade';


const Servicios = () => {
  return (
    <Fragment>
      <Container className="mt-5" id="servicios">
        <Fade bottom>
          <Row className="m-2 my-5">
            <Col>
              <Row>
                <Col lg={3}>
                  <img alt="..." src={ImageMedida} width="200" />
                </Col>
                <Col>
                  <h2 style={{ color: "var(--details)" }}>Productos</h2>
                  <p>
                    Visita nuestra tienda y encontra un amplia gama de productos.
                    Los mejores celulares del mercado y todos los accesorios q
                    necesitas para tener la mejor ezperiencia
                </p>
                  <Link to="/tienda">
                    <Button
                      variant="dark"
                      className="float-right mt-3"
                      style={{ backgroundColor: "var(--details" }}
                    >
                      Visitar tienda
                  </Button>
                  </Link>
                </Col>
              </Row>
            </Col>
          </Row>
        </Fade>
        <Fade bottom>
          <Row className="m-2 my-5">
            <Col>
              <Row>
                <Col lg={3}>
                  <img alt="..." src={ImageRepair} width="200" />
                </Col>
                <Col>
                  <h2 style={{ color: "var(--details)" }}>Reparacion</h2>
                  <p>reparamos cualquier problema con tu celular</p>
                  <Link to="/turnos">
                    <Button
                      variant="dark"
                      className="float-right mt-3"
                      style={{ backgroundColor: "var(--details" }}
                    >
                      Sacar tu turno
                  </Button>
                  </Link>
                </Col>
              </Row>
            </Col>
          </Row>
        </Fade>
        <Fade bottom>
          <Row className="m-2 my-5">
            <Col>
              <Row>
                <Col lg={3}>
                  <img alt="..." src={ImageDelivery} width="200" />
                </Col>
                <Col>
                  <h2 style={{ color: "var(--details)" }}>delivery</h2>
                  <p>
                    Te enviamos tu smartphone nuevo a tu casa
                </p>
                  <Link to="/tienda">
                    <Button
                      variant="dark"
                      className="float-right mt-3"
                      style={{ backgroundColor: "var(--details" }}
                    >
                      Compra ahora!
                  </Button>
                  </Link>
                </Col>
              </Row>
            </Col>
          </Row>
        </Fade>
      </Container>
    </Fragment>
  );
};

export default Servicios;


