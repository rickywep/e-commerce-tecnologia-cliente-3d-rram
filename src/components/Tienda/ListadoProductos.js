import React from "react";
import { Row } from "react-bootstrap";
import Producto from "./Producto";

function ListadoProductos({ setProductosCarrito, productosCarrito, productos, setMostrarModal, mostrarModal }) {
  return (
    <Row className="justify-content-center">
      {productos.map((producto) => (
        <Producto
          setProductosCarrito={setProductosCarrito}
          productosCarrito={productosCarrito}
          key={producto._id}
          producto={producto}
          setMostrarModal={setMostrarModal}
          mostrarModal={mostrarModal}
        />
      ))}
    </Row>
  );
}

export default ListadoProductos;
