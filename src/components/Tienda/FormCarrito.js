import React, { Fragment, useState } from "react";
import { Row, Col, Form, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
const FormCarrito = ({
  usuario,
  setDatosPersona,
  datosPersona,
  provincias,
  detalleTotal,
  envioValor,
}) => {

  const {
    nombre,
    apellido,
    domicilio,
    provincia,
    ciudad,
    codPostal,
    telefono,
  } = datosPersona;

  const [cel, setCel] = useState('')
  const [pos, setPos] = useState('')


  const onChangeCel = (e) => {
    const re = /^[0-9\b]+$/;
    // if value is not blank, then test the regex
    if (e.target.value === '' || re.test(e.target.value)) {
      setCel(e.target.value)
      setDatosPersona({
        ...datosPersona,
        [e.target.name]: e.target.value,
      });
    }
  };
  const onChangePostal = (e) => {
    const re = /^[0-9\b]+$/;
    // if value is not blank, then test the regex
    if (e.target.value === '' || re.test(e.target.value)) {
      setPos(e.target.value)
      setDatosPersona({
        ...datosPersona,
        [e.target.name]: e.target.value,
      });
    }
  };
  const onChangeUsuario = (e) => {
    setDatosPersona({
      ...datosPersona,
      [e.target.name]: e.target.value,
    });
  };
  return (
    <Fragment>
      {usuario ? (
        <Form>
          <Row>
            <Col sm={6}>
              <Form.Group controlId="formRegistroNombre">
                <Form.Control
                  type="text"
                  name="nombre"
                  required
                  placeholder="Ingrese su nombre"
                  onChange={onChangeUsuario}
                  value={nombre}
                  maxLength="30"
                />
              </Form.Group>
            </Col>
            <Col sm={6}>
              <Form.Group controlId="formRegistroApellido">
                <Form.Control
                  type="text"
                  name="apellido"
                  required
                  placeholder="Ingrese su apellido"
                  onChange={onChangeUsuario}
                  value={apellido}
                  maxLength="30"
                />
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col sm={12}>
              <Form.Group controlId="formRegistroDomicilio">
                <Form.Control
                  type="text"
                  name="domicilio"
                  required
                  placeholder="Domicilio"
                  onChange={onChangeUsuario}
                  value={domicilio}
                  maxLength="30"
                />
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col sm={6}>
              <Form.Group controlId="provincias">
                <Form.Control
                  as="select"
                  name="provincia"
                  value={provincia}
                  onChange={onChangeUsuario}
                >
                  <option default required>
                    Selecciona una Provincia...
                  </option>
                  {provincias.map((provincia) => (
                    <option key={provincia.id} value={provincia.nombre}>
                      {provincia.nombre}
                    </option>
                  ))}
                </Form.Control>
              </Form.Group>
            </Col>
            <Col sm={6}>
              <Form.Group controlId="formRegistroCiudad">
                <Form.Control
                  type="text"
                  name="ciudad"
                  required
                  placeholder="Ciudad"
                  onChange={onChangeUsuario}
                  value={ciudad}
                  maxLength="20"
                />
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col sm={6}>
              <Form.Group controlId="formRegistroCodPostal">
                <Form.Control
                  type="text"
                  name="codPostal"
                  required
                  placeholder="Codigo Postal"
                  value={pos}
                  onChange={onChangePostal}
                  maxLength="8"
                />
              </Form.Group>
            </Col>
            <Col sm={6}>
              <Form.Group controlId="formRegistroTelefono">
                <Form.Control
                  type="text"
                  name="telefono"
                  required
                  placeholder="Tel./Celular"
                  onChange={onChangeCel}
                  maxLength="20"
                  value={cel}
                />
              </Form.Group>
            </Col>
          </Row>
          <hr />
          <Row>
            <Col sm={6} className="p-3">
              <Form.Check
                className="ml-5"
                custom
                inline
                label=""
                type="radio"
                checked={detalleTotal.envio === 0.0 ? true : false}
                id={`custom-inline-1`}
                onChange={() => envioValor(0.0)}
              />
              <h5 className="carritoLabel">Envío Gratis</h5>
              <h6 className="carritoLabel1">Entre 2-5 días hábiles</h6>
            </Col>
            <Col sm={6} className="p-3">
              <Form.Check
                className="ml-5"
                custom
                inline
                label=""
                type="radio"
                checked={detalleTotal.envio !== 0.0 ? true : false}
                id={`custom-inline-2`}
                onChange={() => envioValor(20.0)}
              />
              <h5 className="carritoLabel">Entrega al día siguiente - $ 20</h5>
              <h6 className="carritoLabel1">24 horas desde su compra</h6>
            </Col>
          </Row>
          <hr />
        </Form>
      ) : (
          <div>
            <h3 className="m-5" style={{ color: "var(--details)" }}>
              Por Favor logueate para completar tus datos
          </h3>
            <Button
              variant="outline-dark"
              as={Link}
              to="/login"
              className="mx-5 text-white"
              style={{ backgroundColor: "var(--details)" }}
            >
              Ingresar a tu cuenta
          </Button>
          </div>
        )}
    </Fragment>
  );
};

export default FormCarrito;
