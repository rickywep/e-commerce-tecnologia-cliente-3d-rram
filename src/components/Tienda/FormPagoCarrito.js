import React, { useState } from "react";
import {
  Row,
  Col,
  Form,
} from "react-bootstrap";
import Cards from "react-credit-cards";
import "react-credit-cards/es/styles-compiled.css";
import "../../../src/index.css";

const FormPagoCarrito = ({tarjeta, setTarjeta }) => {

  const [focus, setFocus] = useState("");

  const { name } = tarjeta

  const [numero, setNumero] = useState('')
  const [exp, setExp] = useState('')
  const [cv, setCv] = useState('')

  const onChangeNumero = (e) => {

    const re = /^[0-9\b]+$/;
    // if value is not blank, then test the regex
    if (e.target.value === '' || re.test(e.target.value)) {
      setNumero(e.target.value)
    }
  }
  const onChangeCv = (e) => {

    const re = /^[0-9\b]+$/;
    // if value is not blank, then test the regex
    if (e.target.value === '' || re.test(e.target.value)) {
      setCv(e.target.value)
    }
  }
  const onChangeExp = (e) => {

    const re = /^[0-9\b]+$/;
    // if value is not blank, then test the regex
    if (e.target.value === '' || re.test(e.target.value)) {
      setExp(e.target.value)
    }
  }
  const onChangeTarjeta = (e) => {
    setTarjeta({
      ...tarjeta,
      number: numero,
      expiry: exp,
      cvc:cv,
      [e.target.name]: e.target.value,
    });
  }
  return (
    <Form>
      <Row className="mt-2">
        <Cards
          number={numero}
          name={name}
          expiry={exp}
          cvc={cv}
          focused={focus}
        />
      </Row>
      <Row className="mt-3">
        <Col xs={6}>
          <Form.Group controlId="formNumber" className="ml-4">
            <Form.Control
              name="numero"
              type="tel"
              required
              placeholder="0000 0000 0000 0000"
              maxLength="16"
              value={numero}
              onChange={onChangeNumero}
              onFocus={(e) => setFocus(e.target.name)}
            />
          </Form.Group>
        </Col>
        <Col xs={3}>
          <Form.Group controlId="formExpiry">
            <Form.Control
              type="text"
              name="exp"
              required
              placeholder="MM/YY"
              maxLength="4"
              value={exp}
              onChange={onChangeExp}
              onFocus={(e) => setFocus(e.target.name)}
            />
          </Form.Group>
        </Col>
        <Col xs={3}>
          <Form.Group controlId="formCvc">
            <Form.Control
              type="text"
              name="cvc"
              required
              placeholder="CVC"
              maxLength="3"
              value={cv}
              onChange={onChangeCv}

              // onChange={(e) => setCvc(e.target.value)}
              onFocus={(e) => setFocus(e.target.name)}
            />
          </Form.Group>
        </Col>
      </Row>
      <Row className="mt-2">
        <Col xs={12}>
          <Form.Group controlId="formName" className="ml-4">
            <Form.Control
              type="text"
              name="name"
              required
              placeholder="Nombre del titular de la tarjeta"
              maxLength="50"
              value={name}
              onChange={onChangeTarjeta}
              onFocus={(e) => setFocus(e.target.name)}
            />
          </Form.Group>
        </Col>
      </Row>
    </Form>
  );
};

export default FormPagoCarrito;
