import React, { Fragment } from "react";
import { Row,Col,} from "react-bootstrap";
const ResumenCarrito = ({ detalleTotal }) => {
  return (
    <Fragment>
      <h3>Resumen</h3>
      <hr />
      <Row>
        <Col>
          <h5>SUBTOTAL</h5>
          <h5>ENVIO</h5>
          {/* <h5>IMPUESTOS</h5> */}
        </Col>
        <Col>
          <h5 className="d-flex justify-content-end">
            $ {Number.parseFloat(detalleTotal.subtotal).toFixed(2)}{" "}
          </h5>
          <h5 className="d-flex justify-content-end">
            {detalleTotal.envio === 0.0
              ? "GRATIS"
              : "$ " + Number.parseFloat(detalleTotal.envio).toFixed(2)}
          </h5>
          {/* <h5 className="d-flex justify-content-end">
                      $ {Number.parseFloat(detalleTotal.impuesto).toFixed(2)}{" "}
                    </h5> */}
        </Col>
      </Row>
      <hr/> 
        <Row>
          <Col>
            <h3 style={{ color: "var(--details" }}>Total</h3>
          </Col>
          <Col>
            <h4
              className="d-flex justify-content-end"
              style={{ color: "var(--details" }}
            >
              $ {Number.parseFloat(detalleTotal.total).toFixed(2)}{" "}
            </h4>
          </Col>
        </Row>
    </Fragment>
  );
};

export default ResumenCarrito;
