import React, { useState, useEffect } from "react";
import clienteAxios from "../../config/axios";
import { Row, Col, Container, Pagination } from "react-bootstrap";
import Filtro from "./Filtro";
import ListadoProductos from "./ListadoProductos";
import ReactSpiner from "../ReactSpiner";

export default function Tienda({ setProductosCarrito, productosCarrito }) {

  const [mostrarModal, setMostrarModal] = useState(false);
  const [productos, setProductos] = useState([]);
  const [marcas, setMarcas] = useState([]);
  const [cargando, setCargando] = useState(true);
  const [totalPaginas, setTotalPaginas] = useState('')
  const [pagina, setPagina] = useState('')
  const [items, setItems] = useState([])

  const cambiarPage = async (number) => {
    const respuesta = await clienteAxios.get('api/productos?porpagina=12&pagina=' + number);
    const { docs, totalPages, page } = respuesta.data.productos;
    setTotalPaginas(totalPages)
    setPagina(page)
    setProductos(docs);
    setCargando(false);
  };

  useEffect(() => {
    const pageTotal = () => {
      const items1 = [];
      let active = pagina;
      for (let number = 1; number <= totalPaginas; number++) {
        items1.push(
          <Pagination.Item key={number} active={number === active} onClick={() => cambiarPage(number)}>
            {number}
          </Pagination.Item>,
        );
      }
      setItems([items1]);
    }
    pageTotal();
    // eslint-disable-next-line
  }, [pagina, cargando])

  const listarProductos = async () => {
    const respuesta = await clienteAxios.get('api/productos?porpagina=12&pagina=' + 1);
    const { docs, totalPages, page } = respuesta.data.productos;
    setTotalPaginas(totalPages)
    setPagina(page)
    setProductos(docs);
    setCargando(false);
  };
  useEffect(() => {
    listarProductos();

    const listarMarcas = async () => {
      const respuesta = await clienteAxios.get('api/marcas?porpagina=10&pagina=1');
      setMarcas(respuesta.data.marcas.docs);
    };
    listarMarcas();
    // eslint-disable-next-line
  }, [cargando]);

  return (
    <>
      <Container fluid className="contenedor" id="tienda">
        {cargando ? (
          <ReactSpiner />
        ) : (
            <>
              <Row className="bg-light">
                <Col lg={5}>
                  <Filtro marcas={marcas} setCargando={setCargando} setItems={setItems} productos={productos} setProductos={setProductos} />
                </Col>
              </Row>
              <Container className="mt-4">
                <ListadoProductos
                  setProductosCarrito={setProductosCarrito}
                  productosCarrito={productosCarrito}
                  productos={productos}
                  setMostrarModal={setMostrarModal}
                  mostrarModal={mostrarModal}
                />
                <div className="row justify-content-center align-items-center minh-100">
                  <Pagination className="">{items}</Pagination>
                </div>
              </Container>
            </>
          )}
      </Container>
    </>
  );
}
