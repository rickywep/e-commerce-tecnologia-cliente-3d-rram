import React, { Fragment, useState } from "react";
import { Card, Col, Row, Form, Badge, Button } from "react-bootstrap";

function CarritoDetalle({
  cadaProducto,
  productos,
  setProductos,
  indice,
  calcularDetalle,
  setProductosCarrito,
}) {
  const { cantidad, id } = cadaProducto;
  const [cadauno, setCadauno] = useState(cadaProducto.cantidad);
  const changeState = (e) => {
    productos[indice].cantidad = Number(e.target.value);
    setProductos(productos);

    setCadauno(Number(e.target.value));
    calcularDetalle();
  };
  const eliminarProducto = (id) => {
    let c = window.confirm("¿esta seguro que quiere eliminar el producto?");
    if (c === true) {
      let compras = JSON.parse(localStorage.getItem("compras"));
      let compra = compras.filter((producto) => {
        let p
        if (producto.id !== id) {
          p = producto
        }
        return p
      });

      localStorage.setItem("compras", JSON.stringify(compra));
      setProductos(compra);
      setProductosCarrito(compra);
    }
  };

  return (
    <Fragment>
      {cadauno ? null : null}
      <Row className="mt-2">
        <Col xs={2}>
          <Card>
            <Card.Img variant="top" src={cadaProducto.imgCel} />
          </Card>
        </Col>
        <Col xs={6}>
          <h5>{cadaProducto.modelo}</h5>
          <Badge
            className="text-white"
            style={{ backgroundColor: "var(--details)" }}
          >
            ${cadaProducto.precio}
          </Badge>
        </Col>
        <Form>
          <Row className="align-items-center">
            <Col xs={4}>
              {/* <Form.Control
                disabled
                style={{ width: "100px" }}
                name="cadauno"
                type="number"
                step="1"
                min="1"
                max="10"
                placeholder="1"
                value={cantidad}
                onChange={changeState}
              /> */}
              <span>{cantidad}</span>
            </Col>
            <Col>
              <Button
                variant="dark"
                onClick={() => eliminarProducto(id)}
                style={{ backgroundColor: "red" }}
                className="text-white ml-3"
              >
                <i className="fas fa-trash"></i>
              </Button>
            </Col>
          </Row>
        </Form>
        <hr />
      </Row>
    </Fragment>
  );
}

export default CarritoDetalle;
