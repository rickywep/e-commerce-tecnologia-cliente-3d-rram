import React, { Fragment } from 'react'
import { Card, Col, Row } from "react-bootstrap";

function CarritoDetalleMin({cadaProducto, productos, setProductos, indice, calcularDetalle }) {
    // const {id, estadoCel, marca, precio, imgUrl, titulo, caracteristicas, cantidad } = cadaProducto;

    return (
        <Fragment>
            <Row className="mt-2">
                <Col xs={4}>
                    <Card >
                            <Card.Img variant="top" src={cadaProducto.imgUrl} />
                    </Card>
                </Col>
                <Col xs={8}>
                    <h3>{cadaProducto.marca}</h3>
                    <h2>$ {Number.parseFloat(cadaProducto.precio).toFixed(2)}</h2>
                </Col>
                <hr/>
            </Row>
        </Fragment>
    )
}

export default CarritoDetalleMin
