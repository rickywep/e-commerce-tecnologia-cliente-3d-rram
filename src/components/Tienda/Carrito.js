import React, {
  Fragment,
  useState,
  useEffect,
  useCallback,
  useContext,
} from "react";
import { Tabs, Tab, Button, Row, Col, Container, Alert } from "react-bootstrap";
import CarritoDetalle from "./CarritoDetalle";
import ResumenCarrito from "./ResumenCarrito";
import FormCarrito from "./FormCarrito";
import FormPagoCarrito from "./FormPagoCarrito";
import clienteAxios from "../../config/axios";
import LogoChico from "../../images/logos/logo_chico.svg";
import { Link, useHistory } from "react-router-dom";
import AlertaContext from "../../context/alertas/alertaContext";
import AuthContext from "../../context/auth/authContext";

function Carrito({ productosCarrito, setProductosCarrito }) {
  const authContext = useContext(AuthContext);
  const { usuario } = authContext;
  const history = useHistory();
  const alertaContext = useContext(AlertaContext);
  const { alerta, mostrarAlerta } = alertaContext;
  const [isLoading, setLoading] = useState(false);
  
  //state y fn para el manejo de los tabs
  const [tarjeta, setTarjeta] = useState({
    name: "",
    cvc: "",
    expiry: "",
    number: "",
  });

  const [datosCompra, setDatosCompras] = useState({
    nombre: "",
    apellido: "",
    domicilio: "",
    ciudad: "",
    provincia: "",
    productosComprados: productosCarrito,
    total: "",
    codPostal: "",
    telefono: "",
    tipoEnvio: "",
    user: "",
  });
  //state de datos de la persona
  const [datosPersona, setDatosPersona] = useState({
    id: "",
    nombre: "",
    apellido: "",
    domicilio: "",
    provincia: "",
    ciudad: "",
    codPostal: "",
    telefono: "",
  });
  useEffect(() => {
    if (datosPersona.nombre !== "") {
      setDatosCompras({
        ...datosCompra,
        nombre: datosPersona.nombre,
        apellido: datosPersona.apellido,
        domicilio: datosPersona.domicilio,
        provincia: datosPersona.provincia,
        ciudad: datosPersona.ciudad,
        codPostal: datosPersona.codPostal,
        telefono: datosPersona.telefono,
        user: usuario._id
      });
    }
    // eslint-disable-next-line
  }, [datosPersona]);
  const [key, setKey] = useState("home");
  const elegirTab = (tab) => {
    if (
      datosPersona.apellido.trim() === "" ||
      datosPersona.ciudad.trim() === "" ||
      datosPersona.codPostal.trim() === "" ||
      datosPersona.domicilio.trim() === "" ||
      datosPersona.nombre.trim() === "" ||
      datosPersona.provincia.trim() === "" ||
      datosPersona.telefono.trim() === ""
    ) {
      mostrarAlerta("Por favor completa todos los campos", "danger");
      return;
    }
    setKey(tab);
  };

  const [productos, setProductos] = useState(productosCarrito);

  //Detalle cupon subtotal envio impuesto total
  const [detalleTotal, setDetalleTotal] = useState({
    cupon: 0.0,
    subtotal: 0.0,
    envio: 0.0,
    total: 0.0,
    pagoTipo: "Tarjeta",
  });
  useEffect(() => {
    setDatosCompras({
      ...datosCompra,
      tipoEnvio:
        detalleTotal.envio === 20 ? "Al dia siguiente" : "Envio Gratis",
      total: detalleTotal.total,
    });
    // eslint-disable-next-line
  }, [detalleTotal]);
  //state para las provincias
  const [provincias, setProvincias] = useState([]);

  //fn para obtener todas las provoncias
  const consultarProvincias = async () => {
    const solicitud = await fetch(
      "https://apis.datos.gob.ar/georef/api/provincias"
    );
    const respuesta = await solicitud.json();
    setProvincias(
      respuesta.provincias.sort((a, b) => a.nombre.localeCompare(b.nombre))
    );
  };
  //guardo el envio
  const envioValor = (valor) => {
    detalleTotal.envio = valor;
    setDetalleTotal(detalleTotal);
    calcularDetalle();
  };
  //guardo tipo pago
  const envioValorTarjeta = (valor) => {
    detalleTotal.pagoTipo = valor;
    setDetalleTotal(detalleTotal);
    calcularDetalle();
  };
  // Rellenar los campos mientras se tipea
  

  //fn para calcular detalle de compras
  const calcularDetalle = useCallback(() => {
    if (productos) {
      const total1 = productos.reduce(
        (acc, num) => acc + num.cantidad * num.precio,
        0.0
      );
      const subtotal = total1;
      const total = subtotal;
      setDetalleTotal((detalleTotal) => ({
        ...detalleTotal,
        subtotal,
        total: total + detalleTotal.envio,
      }));
    }
  }, [productos]);
  useEffect(() => {
    calcularDetalle();
    consultarProvincias();
    // eslint-disable-next-line
  }, [calcularDetalle, productos]);

  const enviarCompra = async (e) => {
    e.preventDefault();
    handleClick()
    if (
      tarjeta.name.trim() === "" ||
      tarjeta.expiry.trim() === "" ||
      tarjeta.cvc.trim() === "" ||
      tarjeta.number.trim() === ""
    ) {
      mostrarAlerta("completa los datos de tu tarjeta por favor", "danger");
      setLoading(false)
      return;
    }
    const respuesta = await clienteAxios.post("/api/compras", datosCompra);
    if (respuesta) {
      
      let promises =[]
      for (let i = 0; i < productosCarrito.length; i++) {
        const prod = productosCarrito[i];
        let stock = {cantidad: parseInt(prod.cantidad)}
        promises.push(clienteAxios.put(`/api/compras/${prod.id}`, stock))      
      }
      let res = await Promise.all(promises);
      limpiarCarrito();
      setLoading(false)
      history.push("/success");
    }
  };
  const limpiarCarrito = () => {
    // let c = window.confirm("¿esta seguro que quiere eliminar todo el carrito?");
    // if (c === true) {
    localStorage.removeItem("compras");
    setProductosCarrito([]);
    setProductos([]);
  // }
  };
  const handleClick = () => setLoading(true);

  return (
    <Fragment>
      <div className="contenedor">
        {productos && productos.length !== 0 ? (
          <Tabs
            justify
            id="controlled-tab-example"
            activeKey={key}
            onSelect={(k) => setKey(k)}
            className="mt-3"
          >
            <Tab eventKey="home" title="Carrito de Compras">
              <Container>
                <Row className="mt-5 mr-2 ml-2">
                  <Col xs={12} lg={8}>
                    {productos && productos.length !== 0 ? (
                      <div>
                        <h3>Productos</h3>
                        <hr />
                      </div>
                    ) : null}
                    {productos &&
                      productos.map((cadaProducto, index) => (
                        <CarritoDetalle
                          key={index}
                          cadaProducto={cadaProducto}
                          productos={productos}
                          setProductos={setProductos}
                          indice={index}
                          calcularDetalle={calcularDetalle}
                          setProductosCarrito={setProductosCarrito}
                        />
                      ))}
                  </Col>
                  <Col xs={12} lg={4}>
                    {productos && productos.length !== 0 ? (
                      <>
                        <ResumenCarrito
                          detalleTotal={detalleTotal}
                          elegirTab={elegirTab}
                        />
                        <div className="float-right">
                          <Button
                            variant="secondary"
                            className="mt-5 ml-4"
                            onClick={limpiarCarrito}
                          >
                            Limpiar Carrito
                          </Button>
                          <Button
                            variant="dark"
                            className="mt-5 ml-4"
                            onClick={() => setKey("profile")}
                            style={{ backgroundColor: "var(--details" }}
                          >
                            Siguiente
                          </Button>
                        </div>
                      </>
                    ) : (
                      <p> no hay productos en tu carrito</p>
                    )}
                  </Col>
                </Row>
              </Container>
            </Tab>
            <Tab eventKey="profile" title="Detalle de envio">
              <Container>
                <Row className="mt-5 mr-2 ml-2">
                  <Col xs={12} lg={8}>
                    {alerta ? (
                      <Alert
                        variant={alerta.categoria}
                        className="mt-2 text-center"
                      >
                        {alerta.msg}
                      </Alert>
                    ) : null}
                    <FormCarrito
                      usuario={usuario}
                      setDatosPersona={setDatosPersona}
                      datosPersona={datosPersona}
                      provincias={provincias}
                      detalleTotal={detalleTotal}
                      envioValor={envioValor}
                      elegirTab={elegirTab}
                    />
                  </Col>
                  <Col lg={4} xs={12}>
                    <ResumenCarrito detalleTotal={detalleTotal} />
                    <div className="float-right">
                      <Button
                        variant="secondary"
                        className="mt-5 ml-4"
                        onClick={limpiarCarrito}
                      >
                        Limpiar Carrito
                      </Button>
                      <Button
                        variant="dark"
                        className="mt-5 ml-4"
                        type="submit"
                        onClick={() => elegirTab("contact")}
                        style={{ backgroundColor: "var(--details" }}
                      >
                        Siguiente
                      </Button>
                    </div>
                  </Col>
                </Row>
              </Container>
            </Tab>
            <Tab eventKey="contact" title="Forma de pago" disabled>
              <Container>
                <Row className="mt-5 mr-2 ml-2">
                  <Col xs={12} lg={8}>
                    <h4>Pago con tarjeta de credito</h4>
                    <hr />
                    {alerta ? (
                      <Alert
                        variant={alerta.categoria}
                        className="mt-2 text-center"
                      >
                        {alerta.msg}
                      </Alert>
                    ) : null}
                    <FormPagoCarrito
                      detalleTotal={detalleTotal}
                      envioValorTarjeta={envioValorTarjeta}
                      tarjeta={tarjeta}
                      setTarjeta={setTarjeta}
                    />
                  </Col>
                  <Col xs={12} lg={4}>
                    <ResumenCarrito detalleTotal={detalleTotal} />
                    <div className="float-right">
                      <Button
                        variant="secondary"
                        className="mt-5 ml-4"
                        onClick={limpiarCarrito}
                      >
                        Limpiar Carrito
                      </Button>
                      <Button
                        disabled={isLoading}
                        onClick={!isLoading ? enviarCompra : null}
                        variant="dark"
                        className="mt-5 ml-4"
                        style={{ backgroundColor: "var(--details" }}
                      >
                      {isLoading ? "Cargando…" : "Pagar Ahora"}
                      </Button>
                    </div>
                  </Col>
                </Row>
              </Container>
            </Tab>
          </Tabs>
        ) : (
          <>
            <div className="container">
              <div className="row justify-content-center align-items-center minh-100">
                <div className="col-lg-12">
                  <div>
                    <h2 className="text-center m-5">
                      No hay productos en tu carrito
                    </h2>
                    <img
                      className="img-fluid rounded mx-auto d-block"
                      src={LogoChico}
                      alt="logo"
                      width="100"
                    />
                  </div>
                  <div>
                    <Link className="nav-link" to="/tienda">
                      <p
                        style={{ fontSize: "24px", color: "var(--details)" }}
                        className="text-center mt-3"
                      >
                        Visita nuestra tienda
                      </p>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </>
        )}
      </div>
    </Fragment>
  );
}

export default Carrito;
