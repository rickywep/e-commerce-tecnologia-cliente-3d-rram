import React from "react";
import clienteAxios from "../../config/axios";
import {
  Dropdown,
  NavItem,
  Navbar,
  Nav,
} from "react-bootstrap";

function Filtro({setCargando, marcas, setProductos, setItems }) {


  const filtrarMenorPrecio = async () => {
    // let productosOrdenados = [...productos]
    // productosOrdenados.sort((a, b) => a.precio - b.precio)
    // setProductos(productosOrdenados);
    const respuesta = await clienteAxios.get(`/api/productos/menorPrecio`);
    setItems([])
    setProductos(respuesta.data.productos);
  };
  const filtrarMayorPrecio = async () => {
    // let productosOrdenados = [...productos]
    // productosOrdenados.sort((a, b) => b.precio - a.precio)
    // setProductos(productosOrdenados);
    const respuesta = await clienteAxios.get(`/api/productos/mayorPrecio`);
    setItems('')
    setProductos(respuesta.data.productos);
  };
  const filtrarMarca = async (marcaid) => {
    const respuesta = await clienteAxios.get(`/api/productos/porMarca/${marcaid}`);
    setItems('')
    setProductos(respuesta.data.productos);
  };
  const verTodos =  () => {
    setCargando(true)
  }

  return (
    <Navbar bg="light" variant="light">
      <Nav className="mr-auto">
        <Dropdown as={NavItem} className="m-3">
          <Dropdown.Toggle >Ordenar</Dropdown.Toggle>
          <Dropdown.Menu>
            <Dropdown.Item onClick={filtrarMenorPrecio}>Menor Precio</Dropdown.Item>
            <Dropdown.Item onClick={filtrarMayorPrecio}>Mayor Precio</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
        <Dropdown as={NavItem} className="m-3">
          <Dropdown.Toggle >Marca</Dropdown.Toggle>
          <Dropdown.Menu>
            <Dropdown.Item onClick={verTodos}>Ver todos</Dropdown.Item>
            {marcas.map((marca, i) => (
              <Dropdown.Item as="button" onClick={() => { filtrarMarca(marca._id) }} key={marca._id}>{marca.marca}</Dropdown.Item>
            ))}
          </Dropdown.Menu>
        </Dropdown>
      </Nav>
    </Navbar>
  );
}

export default Filtro;
