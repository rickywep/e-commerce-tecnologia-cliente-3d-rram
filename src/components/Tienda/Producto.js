import React, { useState, useEffect } from "react";
import { Row, Col, Badge, Modal, Button, Image, Form } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faShoppingCart } from "@fortawesome/free-solid-svg-icons";


function Producto({ producto, setProductosCarrito }) {
  let compras = JSON.parse(localStorage.getItem("compras"));
  if (!compras) {
    compras = [];
  }

  const [enCarrito, setEnCarrito] = useState(false)
  const [show, setShow] = useState(false);
  const [productoSeleccionado, setProductoSeleccionado] = useState({
    cantidad: 1,
    caracteristicas: "",
    estadoCel: "",
    marca: "",
    modelo: "",
    precio: "",
    id: "",
  });
  const handleClose = () => {
    setShow(false);
  };
  const { _id, imgCel, modelo, caracteristicas, precio, stock } = producto;
  const handleShow = () => {
    let estaEncart = compras.findIndex(item => item.id === _id)
    if (estaEncart > -1) {
      setEnCarrito(true)
    } else {
      setEnCarrito(false)
    }
    setShow(true)
  };
  const eliminarProducto = (id) => {
    let c = window.confirm("¿esta seguro que quiere eliminar el producto?");
    if (c === true) {
      let compras = JSON.parse(localStorage.getItem("compras"));
      let compra = compras.filter((producto) => {
        let p
        if (producto.id !== id) {
          p = producto
        }
        return p
      });

      localStorage.setItem("compras", JSON.stringify(compra));
      setProductosCarrito(compra)
      handleClose();
    }
  };
  const agregarProductoAlCarrito = () => {
    setProductoSeleccionado({
      ...productoSeleccionado,
      caracteristicas: caracteristicas,
      estadoCel: producto.estadoCel,
      marca: producto.marcaid.marca,
      imgCel: imgCel,
      modelo: modelo,
      precio: producto.precio,
      id: _id,
    });
    

    handleClose();
  };
  const changeCantidadProducto = (e) => {
    parseInt(e.target.value) <= stock && 
    setProductoSeleccionado({
      ...productoSeleccionado,
      cantidad: e.target.value,
    });
  };
  useEffect(() => {
    if (productoSeleccionado.modelo !== "") {

      compras.push(productoSeleccionado);
      localStorage.setItem("compras", JSON.stringify(compras));
      setProductosCarrito(compras)
    }
    // eslint-disable-next-line
  }, [productoSeleccionado]);

  return (
    <>
      <Col xs={12} lg={5} className="m-4">
        <Row
          className="bg-light m-3"
          style={{ cursor: "pointer" }}
          onClick={handleShow}
        >
          <Col xs={4}>
            <div className="bg-white m-2">
              <img
                src={imgCel}
                alt="product"
                width="80"
                className="p-1"
              />
            </div>
          </Col>

          <Col xs={8}>
            <Row style={{ color: 'var(--details)' }} className="mt-2">
              <h5 className="ml-3">{modelo}</h5>
            </Row>
            <p className="cortar col-sm-1 col-md-12">{caracteristicas}</p>
            <Badge style={{ backgroundColor: 'var(--details)', color: 'white' }} className="float-right mt-1">${precio}</Badge>
          </Col>
        </Row>

        <Modal
          show={show}
          onHide={handleClose}
        >
          <Modal.Header closeButton>
            <Modal.Title
              id="example-custom-modal-styling-title"
              style={{ color: "var(--details)" }}
            >
              {modelo}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <Col xs={4}>
                <Image
                  src={imgCel}
                  rounded
                  fluid 
                />
              </Col>
              <Col xs={8} className="p-3 px-5">
                <Row>{caracteristicas}</Row>
                <Badge style={{ backgroundColor: 'var(--details)', color: 'white' }} className="float-right mt-1">Precio: ${precio}</Badge>
              </Col>
            </Row>
            <hr />
            {
              stock === 0 ?
                <p>
                  no hay stock disponible
                  </p>
                :
                <Row className="justify-content-end ">
                  <Col>
                    <p>
                      Cantidad Disponible: {stock}
                    </p>
                  </Col>
                  {
                    !enCarrito ?
                    <>
                      <Col xs={3}>
                        <Form.Control
                          name="cantidad"
                          type="number"
                          step="1"
                          min="1"
                          max={stock}
                          placeholder="1"
                          onChange={changeCantidadProducto}
                        />
                      </Col>
                      <Button
                        onClick={() => {
                          agregarProductoAlCarrito();
                        }}
                        className="text-white float-right mr-3"
                        style={{ backgroundColor: "var(--details)" }}
                      >
                        Agregar a carrito<FontAwesomeIcon icon={faShoppingCart} />
                      </Button>
                      </>
                  :
                      <>
                        <Button
                          variant="success"
                          disabled
                          className="text-white float-right "
                        >
                          ya en carrito<FontAwesomeIcon icon={faShoppingCart} />
                        </Button>
                        <Button
                          variant="dark"
                          onClick={() => eliminarProducto(_id)}
                          style={{ backgroundColor: "red" }}
                          size="sm"
                          className="text-white mx-2"
                        >
                          <i className="fas fa-trash"></i>
                        </Button>
                      </>
                  }
                </Row>

            }
          </Modal.Body>
        </Modal>
      </Col>
    </>
  );
}

export default Producto;
