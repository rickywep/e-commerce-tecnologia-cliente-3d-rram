import React from "react";
import { Row, Col, Button } from "react-bootstrap";
import ImageRepair from "../images/Servicios/repair.svg";
import ImageDelivery from "../images/Servicios/delivery.svg";
import ImageMedida from "../images/Servicios/medida.svg";
import { Link } from "react-router-dom";
const ServiciosHome = () => {
  return (
    <div className="my-5 py-5">
      <h2 className="my-5">Servicios que ofrecemos</h2>
      <Row>
        <Col sm={12} lg={4}>
          <Row className="bg-light shadow p-2 m-1">
            <Col>
              <img
                alt="..."
                src={ImageMedida}
                width="150"
                style={{ height: "10rem" }}
              />
            </Col>
            <Col>
              <h4 style={{ color: "var(--details)" }}>Productos</h4>
              <p>Celulares a tu medida</p>
              <Link to="/tienda">
                <Button
                  variant="dark"
                  className="float-right mt-3"
                  style={{ backgroundColor: "var(--details" }}
                >
                  Visitar tienda
                </Button>
              </Link>
            </Col>
          </Row>
        </Col>
        <Col sm={12} lg={4}>
          <Row className="bg-light shadow p-2 m-1">
            <Col>
              <img
                alt="..."
                src={ImageRepair}
                width="200"
                style={{ height: "10rem" }}
              />
            </Col>
            <Col>
              <h4 style={{ color: "var(--details)" }}>Reparacion</h4>

              <p>El mejor Servicio tecnico</p>
              <Link to="/turnos">
                <Button
                  variant="dark"
                  className="float-right mt-3"
                  style={{ backgroundColor: "var(--details" }}
                >
                  Sacar tu turno
                </Button>
              </Link>
            </Col>
          </Row>
        </Col>
        <Col sm={12} lg={4}>
          <Row className="bg-light shadow p-2 m-1">
            <Col>
              <img
                alt="..."
                src={ImageDelivery}
                width="200"
                style={{ height: "10rem" }}
              />
            </Col>
            <Col>
              <h4 style={{ color: "var(--details)" }}>Delivery</h4>

              <p>
                Te enviamos tu smartphone
              </p>
              <Link to="/turnos">
                <Button
                  variant="dark"
                  className="float-right mt-3"
                  style={{ backgroundColor: "var(--details" }}
                >
                  Conocer mas
                </Button>
              </Link>
            </Col>
          </Row>
        </Col>
      </Row>
      {/* <Link to="/servicios">
        <Button
          variant="dark"
          className="float-right mt-3 ml-5"
          style={{ backgroundColor: "var(--details" }}
        >
          Saber mas
        </Button>
      </Link> */}
    </div>
  );
};

export default ServiciosHome;
