import React, { Fragment, useState, useEffect, useContext } from "react"
import { Row, Col, ListGroup, Container } from "react-bootstrap";
import Abmmarcas from '../productos/Abmmarcas';
import Abmproductos from '../productos/Abmproductos';
import AuthContext from "../../context/auth/authContext";


function Productos() {
    const authContext = useContext(AuthContext);
    const { usuario } = authContext;
    //state de marcas o productos
    const [abmtipo, setAbmtipo] = useState("Marcas");
    //state que contiene marcas
    const [marcas, setMarcas] = useState([]);
    // state de errores
    const [errores, setErrores] = useState('');
    //state para el pagination
    const page = { pagina: 1, porpagina: 10 }
    const [datospageprod, setDatospageprod] = useState([]);
    // Obtener las Marcas
    const obtenerMarcas = async () => {
        const solicitud = await fetch(`${process.env.REACT_APP_BACKEND_URL}/api/marcas?porpagina=` + page.porpagina + '&pagina=' + page.pagina, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'x-auth-token': localStorage.getItem('token')
            }
        });

        const respuesta = await solicitud.json();
        if (solicitud.ok) {
            setMarcas(respuesta.marcas);
        } else {
            setErrores(respuesta.msg);
        }
    }
    //state que contiene los productos
    const [productos, setProductos] = useState([]);
    const obtenerProductos = async () => {
        const solicitud = await fetch(`${process.env.REACT_APP_BACKEND_URL}/api/productos?porpagina=` + page.porpagina + '&pagina=' + page.pagina, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'x-auth-token': localStorage.getItem('token')
            }
        });
        const respuesta = await solicitud.json();
        if (solicitud.ok) {
            setProductos(respuesta.productos.docs);
            const { docs, ...resto } = respuesta.productos;
            setDatospageprod(resto);
        } else {
            setErrores(respuesta.msg);
        }
    }
    // Se selecciona alta de marcas o productos
    const onClickTipo = (tipo) => {
        if (tipo === 'Productos') {
            const obtenerMarcas = async () => {
                const solicitud = await fetch(`${process.env.REACT_APP_BACKEND_URL}/api/marcas?porpagina=100&pagina=1`, {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'x-auth-token': localStorage.getItem('token')
                    }
                });
                const respuesta = await solicitud.json();
                if (solicitud.ok) {
                    setMarcas(respuesta.marcas);
                } else {
                    setErrores(respuesta.msg);
                }
            }
            obtenerMarcas();
        } else {
            const obtenerMarcas = async () => {
                const solicitud = await fetch(`${process.env.REACT_APP_BACKEND_URL}/api/marcas?porpagina=` + page.porpagina + '&pagina=' + page.pagina, {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'x-auth-token': localStorage.getItem('token')
                    }
                });

                const respuesta = await solicitud.json();
                if (solicitud.ok) {
                    setMarcas(respuesta.marcas);
                } else {
                    setErrores(respuesta.msg);
                }
            }
            obtenerMarcas();
        }

        setAbmtipo(tipo);
    }
    useEffect(() => {
        obtenerMarcas();
        obtenerProductos();
        // eslint-disable-next-line
    }, []);

    return (
        <Fragment>
            {usuario &&

                <Container>
                    <Row>
                        <Col>
                            <ListGroup.Item
                                as="button"
                                className="list-group-item-action mt-4"
                                style={abmtipo === "Marcas" ? { backgroundColor: 'var(--details)', color: 'white' } : null}
                                onClick={() => onClickTipo("Marcas")}
                            > Marcas
                    </ListGroup.Item>
                        </Col>
                        <Col>
                            <ListGroup.Item
                                as="button"
                                className="list-group-item-action mt-4"
                                style={abmtipo === "Productos" ? { backgroundColor: 'var(--details)', color: 'white' } : null}
                                onClick={() => onClickTipo("Productos")}
                            > Productos
                    </ListGroup.Item>
                        </Col>
                    </Row>
                    <hr className="my-5" />
                    {abmtipo === "Marcas" ?
                        <Abmmarcas
                            marcas={marcas}
                            setMarcas={setMarcas}
                            errores={errores}
                            setErrores={setErrores}
                            page={page}
                        />
                        :
                        <Abmproductos
                            productos={productos}
                            setProductos={setProductos}
                            marcas={marcas.docs}
                            errores={errores}
                            setErrores={setErrores}
                            page={page}
                            datospageprod={datospageprod}
                            setDatospageprod={setDatospageprod}
                        />}
                </Container>}
        </Fragment>
    )
}

export default Productos

