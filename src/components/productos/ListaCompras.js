import React, {useState} from 'react';
import { Form, Col, Row, Button, Table, Container } from 'react-bootstrap';
import moment from 'moment'

function ListaCompras({proveedores, setProveedores, errores, setErrores}) {
    let date = moment().format('YYYY-MM-DD');
    // state para cargar los datos para la consulta
    const [listCompras, setListCompras] = useState({proveedorid:"", diainicial: date, diafinal: date});
    const {diainicial, diafinal} = listCompras;
    // state que contiene el listado obtenido
    const [ compras, setCompras ] = useState([]);

    // cuando cambian los campos
    const onChangelistCompras = e => {
        setListCompras({
            ...listCompras,
            [e.target.name]: e.target.value
        });
    };
    // fn del listado
    const ListarCompras = async () => {
        const solicitud = await fetch(`${process.env.REACT_APP_BACKEND_URL}/api/compralist`, {
            method: 'GET',
            headers:{
                'Content-Type': 'application/json',
                'x-auth-token': localStorage.getItem('token'),
                'proveedorid': listCompras.proveedorid,
                'diainicial': listCompras.diainicial,
                'diafinal': listCompras.diafinal 
            }
        });
        const respuesta = await solicitud.json();
        if (solicitud.ok) {
            setCompras(respuesta.compras);
        } else {
            alert(respuesta.msg);
        }
    }


    const onSubmitListarCompras = e => {
        e.preventDefault();
        // Validaciones
        if(listCompras.diainicial.trim() === '' || listCompras.diafinal.trim() === '' ){
            alert('Debe completar los Campos requeridos!');
            return;
        }
        // ejecuto fn del listado
        ListarCompras();
    }

    return (
        <Container>
            <Row className="mt-5">
                <Form className="w-100 mb-5" onSubmit={onSubmitListarCompras}>
                    <Row>
                        <Col sm={4}>
                            <Form.Group controlId="formproveedor">
                                <Form.Control as="select" value={listCompras.proveedorid} name="proveedorid" onChange={onChangelistCompras} >
                                <option default>Todos...</option>
                                {proveedores.map((prove) => (
                                    <option key={prove._id} value={prove._id}>
                                    {prove.nombre}
                                    </option>
                                ))}
                                </Form.Control>
                                <Form.Text className="text-muted">
                                Ingrese el Proveedor.
                                </Form.Text>
                            </Form.Group>
                        </Col>
                        <Col sm={4}>
                            <Form.Group controlId="formdiainicial">
                                <Form.Control 
                                    type="date"
                                    name="diainicial"
                                    required
                                    placeholder="Ingrese la diainicial"
                                    onChange={onChangelistCompras}
                                    value={diainicial}
                                />
                                <Form.Text className="text-muted">
                                Ingrese fecha de inicio.
                                </Form.Text>
                            </Form.Group>
                        </Col>
                        <Col sm={4}>
                            <Form.Group controlId="formdiafinal">
                                <Form.Control 
                                    type="date"
                                    name="diafinal"
                                    required
                                    placeholder="Ingrese la diafinal"
                                    onChange={onChangelistCompras}
                                    value={diafinal}
                                />
                                <Form.Text className="text-muted">
                                Ingrese fecha final.
                                </Form.Text>
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row className="justify-content-center">
                        <Col sm={3}>
                            <Button 
                                variant="primary"
                                type="submit"
                                block
                            > Generar Listado
                            </Button>
                        </Col>
                    </Row>
                </Form>
            </Row>
            <Row className="d-flex justify-content-center">
                    {
                        compras.length > 0 ?
                            <Table responsive striped bordered hover>
                                <thead>
                                    <tr>
                                    <th>Proveedor</th>
                                    <th>Marca</th>
                                    <th>Modelo</th>
                                    <th>Fecha Compra</th>                                    
                                    <th>Cantidad</th>
                                    <th>Precio Compra</th>
                                    <th>Precio Venta</th>
                                    <th>Precio Anterior</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {compras.map( comp => (
                                    <tr>
                                    <td>{ comp.proveedorid.nombre }</td>
                                    <td>{ comp.productoid.marcaid.marca }</td>
                                    <td>{ comp.productoid.modelo }</td>
                                    <td>{ moment(comp.created_at).format('DD-MM-YYYY') }</td>
                                    <td>{ comp.cantidad }</td>
                                    <td>{ comp.preciocompra }</td>
                                    <td>{ comp.precioventa }</td>
                                    <td>{ comp.precioanterior }</td>
                                    </tr>
                                    )
                                    )}
                                </tbody>
                            </Table>
                        : null
                    }
            </Row>
        </Container>
    )
}

export default ListaCompras
