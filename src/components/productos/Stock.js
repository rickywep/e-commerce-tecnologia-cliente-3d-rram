import React, { useState, useEffect, useContext } from "react"
import { Row, Col, ListGroup, Container, Alert } from "react-bootstrap";
import Abmproveedor from '../productos/Abmproveedor'
import Compras from '../productos/Compras'
import ListaCompras from '../productos/ListaCompras'
import AuthContext from "../../context/auth/authContext";


function Stock() {
    const authContext = useContext(AuthContext);
    const { usuario } = authContext;
    //state de proveedor compras o listados de compras
    const [abmtipo, setAbmtipo] = useState("Proveedor");
    //state de proveedores
    const [proveedores, setProveedores] = useState([]);
    // state de errores
    const [errores, setErrores] = useState('');
    // Obtener las Proveedores
    const obtenerProveedores = async () => {
        const solicitud = await fetch(`${process.env.REACT_APP_BACKEND_URL}/api/proveedor`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'x-auth-token': localStorage.getItem('token')
            }
        });
        const respuesta = await solicitud.json();
        if (solicitud.ok) {
            setProveedores(respuesta.proveedores);
        } else {
            setErrores(respuesta.msg);
        }
    }
    // Se selecciona alta de proveedor o compras o listados de compras
    const onClickTipo = (tipo) => {
        setAbmtipo(tipo);
    }
    useEffect(() => {
        obtenerProveedores();
        //obtenerProductos();
    }, []);

    return (
        <Container>
            {usuario &&
                <>
                    <Row>
                        <Col>
                            <ListGroup.Item
                                as="button"
                                className="list-group-item-action mt-4"
                                style={abmtipo === "Proveedor" ? { backgroundColor: 'var(--details)', color: 'white' } : null}
                                onClick={() => onClickTipo("Proveedor")}
                            > ABM Proveedor
                    </ListGroup.Item>
                        </Col>
                        <Col>
                            <ListGroup.Item
                                as="button"
                                className="list-group-item-action mt-4"
                                style={abmtipo === "Compras" ? { backgroundColor: 'var(--details)', color: 'white' } : null}
                                onClick={() => onClickTipo("Compras")}
                            > Ingreso Stock
                    </ListGroup.Item>
                        </Col>
                        <Col>
                            <ListGroup.Item
                                as="button"
                                className="list-group-item-action mt-4"
                                style={abmtipo === "Listados de Compras" ? { backgroundColor: 'var(--details)', color: 'white' } : null}
                                onClick={() => onClickTipo("Listados de Compras")}
                            > Listados Compras
                    </ListGroup.Item>
                        </Col>
                    </Row>
                    <Row >
                        <div className="w-100 px-5">
                            {errores ? (
                                <Alert variant='danger' className="text-center">
                                    {errores}
                                </Alert>
                            ) : null}
                        </div>
                        <Col>
                            {abmtipo === "Proveedor" ? <Abmproveedor
                                proveedores={proveedores}
                                setProveedores={setProveedores}
                                errores={errores}
                                setErrores={setErrores} />
                                : (abmtipo === "Compras" ?
                                    <Compras
                                        proveedores={proveedores}
                                        setProveedores={setProveedores}
                                        errores={errores}
                                        setErrores={setErrores} />
                                    :
                                    <ListaCompras
                                        proveedores={proveedores}
                                        setProveedores={setProveedores}
                                        errores={errores}
                                        setErrores={setErrores} />
                                )}
                        </Col>
                    </Row>
                </>
            }
        </Container>
    )
}

export default Stock;
