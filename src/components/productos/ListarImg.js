import React, { Fragment} from 'react'
import { Card, Col, Row } from "react-bootstrap";

function ListarImg({cadaProducto, marcas}) {
    //const resul = marcas.find( mar => mar._id === cadaProducto.marcaid);
    //marca aux
    //const [aux, setAux] = useState(resul);
    return (
        <Fragment>
            <Row className="mt-2">
                <Col xs={4}>
                    <Card >
                            <Card.Img variant="top" src={cadaProducto.imgCel} />
                    </Card>
                </Col>
                <Col xs={8}>
                    <h3>{cadaProducto.marcaid.marca}</h3>
                    <h2>$ {Number.parseFloat(cadaProducto.precio).toFixed(2)}</h2>
                    <h2>{cadaProducto.modelo}</h2>
                    <h4>{cadaProducto.caracteristicas}</h4>
                </Col>
                <hr/>
            </Row>
        </Fragment>
    )
}

export default ListarImg