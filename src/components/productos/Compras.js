import React, { useState, useEffect } from 'react'
import { Row, Form, Button, Col, Container } from 'react-bootstrap';

function Compras({ proveedores, setErrores }) {
    //state de productos
    const [productos, setProductos] = useState([]);
    //state de compra
    const [compra, setCompra] = useState({
        proveedorid: "",
        productoid: "",
        cantidad: 0,
        preciocompra: 0.00,
        cambiaprecio: true,
        precioventa: 0.00,
        precioanterior: 0.00
    });
    const {
        cantidad,
        preciocompra,
        precioventa,
        precioanterior } = compra;
    // Cuando cambian los campos
    const onChangeCompra = e => {
        if (e.target.name === "productoid") {
            if (e.target.value !== 'Seleccionar Producto...') {
                const produelegido = productos.find(p => p._id === e.target.value);
                compra.precioanterior = produelegido.precio;
            } else {
                compra.precioanterior = 0.00;
            }
        }
        setCompra({
            ...compra,
            [e.target.name]: e.target.value
        });
    };
    //funcion para guardar la compra
    const agregarCompra = async () => {
        const solicitud = await fetch(`${process.env.REACT_APP_BACKEND_URL}/api/compra`, {
            method: 'POST',
            body: JSON.stringify(compra),
            headers: {
                'Content-Type': 'application/json',
                'x-auth-token': localStorage.getItem('token')
            }
        });
        const respuesta = await solicitud.json();
        if (solicitud.ok) {
            setCompra({
                proveedorid: "",
                productoid: "",
                cantidad: 0,
                preciocompra: 0.00,
                cambiaprecio: true,
                precioventa: 0.00,
                precioanterior: 0.00
            });
        }
        alert(respuesta.msg);
    }
    // valido compra y ejecuto fn de guardado
    const onSubmitNuevacompra = e => {
        e.preventDefault();
        // Validaciones 
        if (compra.proveedorid.trim() === '' || compra.proveedorid.trim() === 'Seleccionar Proveedor...' || compra.productoid.trim() === '' || compra.productoid.trim() === 'Seleccionar Producto...' || compra.cantidad === 0.00 || compra.preciocompra === 0.00 || compra.precioventa === 0.00) {
            alert('Debe completar los Campos requeridos!');
            return;
        }
        // Agregar compra
        agregarCompra();
    };
    const obtenerProductos = async () => {
        const solicitud = await fetch(`${process.env.REACT_APP_BACKEND_URL}/api/productos?porpagina=100&pagina=1`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'x-auth-token': localStorage.getItem('token')
            }
        });        
        const respuesta = await solicitud.json();
        if (solicitud.ok) {
            setProductos(respuesta.productos.docs);
        } else {
            setErrores(respuesta.msg);
        }
    }
    //leo todos los productos de mongoose
    useEffect(() => {
        obtenerProductos();
        // eslint-disable-next-line
    }, []);
    return (
        <Container>
            <Row className="d-flex justify-content-center">
                <Form className="w-100 mt-5 mb-5" onSubmit={onSubmitNuevacompra}>
                    <Row>
                        <Col sm={4}>
                            <Form.Group >
                                <Form.Control as="select" value={compra.proveedorid} name="proveedorid" onChange={onChangeCompra} >
                                    <option default>Seleccionar Proveedor...</option>
                                    {proveedores.map((prove) => (
                                        <option key={prove._id} value={prove._id}>
                                            {prove.nombre}
                                        </option>
                                    ))}
                                </Form.Control>
                                <Form.Text className="text-muted">
                                    Ingrese el Proveedor.
                                </Form.Text>
                            </Form.Group>
                        </Col>
                        <Col sm={5}>
                            <Form.Group >
                                <Form.Control as="select" value={compra.productoid} name="productoid" onChange={onChangeCompra} >
                                    <option default>Seleccionar Producto...</option>
                                    {productos.map((produ) => (
                                        <option key={produ._id} value={produ._id}>
                                            {produ.marcaid.marca + " -- " + produ.modelo}
                                        </option>
                                    ))}
                                </Form.Control>
                                <Form.Text className="text-muted">
                                    Ingrese el Producto.
                                </Form.Text>
                            </Form.Group>
                        </Col>
                        <Col sm={3}>
                            <Form.Group >
                                <Form.Control
                                    type="number"
                                    name="cantidad"
                                    required
                                    placeholder="Ingrese la Cantidad"
                                    onChange={onChangeCompra}
                                    value={cantidad}
                                />
                                <Form.Text className="text-muted">
                                    Ingrese la cantidad.
                                </Form.Text>
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={3}>
                            <Form.Group >
                                <Form.Control
                                    type="number"
                                    name="preciocompra"
                                    id="preciocompra"
                                    min="0.00"
                                    placeholder="Precio de Compra"
                                    onChange={onChangeCompra}
                                    value={preciocompra}
                                />
                                <Form.Text className="text-muted">
                                    Ingrese el Precio de compra.
                                </Form.Text>
                            </Form.Group>
                        </Col>
                        <Col sm={3}>
                            <Form.Group >
                                <Form.Control
                                    type="number"
                                    name="precioventa"
                                    id="precioventa"
                                    min="0.00"
                                    placeholder="Precio de Venta"
                                    onChange={onChangeCompra}
                                    value={precioventa}
                                />
                                <Form.Text className="text-muted">
                                    Ingrese el Precio de Venta.
                                </Form.Text>
                            </Form.Group>
                        </Col>
                        <Col sm={3}>
                            <Form.Group >
                                <Form.Control as="select" value={compra.cambiaprecio} name="cambiaprecio" onChange={onChangeCompra}>
                                    <option default>Cambia Precio?</option>
                                    <option key={1} value={true} >Si</option>
                                    <option key={2} value={false} >No</option>
                                </Form.Control>
                                <Form.Text className="text-muted">
                                    Cambia el precio de venta.
                                </Form.Text>
                            </Form.Group>
                        </Col>
                        <Col sm={3}>
                            <Form.Group >
                                <Form.Control
                                    type="number"
                                    name="precioanterior"
                                    id="precioanterior"
                                    readOnly
                                    placeholder="Precio Anterior"
                                    onChange={onChangeCompra}
                                    value={precioanterior}
                                />
                                <Form.Text className="text-muted">
                                    Ultimo precio de venta.
                                </Form.Text>
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row className="justify-content-center">
                        <Col sm={3}>
                            <Button
                                variant="primary"
                                type="submit"
                                block
                            > Agregar Compra
                            </Button>
                        </Col>
                    </Row>
                </Form>
            </Row>
        </Container>
    )
}

export default Compras
