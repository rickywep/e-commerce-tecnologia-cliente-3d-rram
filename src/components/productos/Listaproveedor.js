import React, { Fragment, useState } from 'react'
import { Button, ButtonGroup, Modal, Form } from 'react-bootstrap';

function Listaproveedor({ proveedor, proveedores, setProveedores, ModificarProveedor }) {
    //state y fn del modal
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    // Eliminar un proveedor
    const eliminarProveedor = async (id) => {
        const solicitud = await fetch(`${process.env.REACT_APP_BACKEND_URL}/api/proveedor/` + id, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'x-auth-token': localStorage.getItem('token')
            }
        });
        const respuesta = await solicitud.json();
        if (solicitud.ok) {
            setProveedores([...proveedores.filter(p => p._id !== proveedor._id)]);
        }
        alert(respuesta.msg);
    }

    // Eliminar el proveedor
    const onClickEliminar = () => {
        if (window.confirm('¿Esta seguro que desea Eliminar el Proveedor?')) {
            eliminarProveedor(proveedor._id);
        }
    }
    return (
        <Fragment>
            <tr>
                <td>{proveedor.nombre}</td>
                <td>{proveedor.email}</td>
                <td>{proveedor.celular}</td>
                <td><Button variant="warning" className="badge" onClick={handleShow}>
                    Ver +
            </Button></td>
                <td><ButtonGroup size="sm" className="ml-2">
                    <Button
                        variant="secondary"
                        onClick={() => ModificarProveedor(proveedor)}
                    >Modificar</Button>
                    <Button
                        variant="dark"
                        onClick={onClickEliminar}
                    >Borrar</Button>
                </ButtonGroup>
                </td>
            </tr>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Observaciones</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Group controlId="observaciones">
                        <Form.Control
                            as="textarea"
                            rows="3"
                            name="observaciones"
                            readOnly
                            placeholder="Ingrese las observaciones del proveedor"
                            value={proveedor.observaciones}
                        />
                    </Form.Group>
                </Modal.Body>
            </Modal>
        </Fragment>
    )
}

export default Listaproveedor
