import React, { useState, Fragment } from 'react';
import { Button, ButtonGroup, Modal } from 'react-bootstrap';
import ListarImg from '../productos/ListarImg'

function Listaproductos({producto1, productos, setProductos, FormModificarProducto, marcas, index, errores, setErrores, modificarProducto}) {
    //const resul = marcas.find( mar => mar._id === producto.marcaid);
    //marca aux
    //const [aux, setAux] = useState(resul);
    //state y fn del modal
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    
    // Elimina un producto
    const eliminarProducto = async(id) => {
        const solicitud = await fetch(`${process.env.REACT_APP_BACKEND_URL}/api/productos/` + id, {
            method: 'DELETE',
            headers:{
                'Content-Type': 'application/json',
                'x-auth-token': localStorage.getItem('token')
            }
        });
        const respuesta = await solicitud.json();
        if (solicitud.ok) {
            setProductos([...productos.filter(p => p._id !== producto1._id)]);
        } else {
            //setErrores(respuesta.msg);
        }
        alert(respuesta.msg);
    }
    // Eliminar producto
    const onClickEliminar = () => {
        if(window.confirm('¿Esta seguro que desea eliminar el Producto?')){
            eliminarProducto(producto1._id);
            //setProductos([...productos.filter(p => p.id !== producto.id)]);
        }
    }
    const onClickDisponible = () => {
        producto1.disponible = !producto1.disponible;
        producto1.marcaid = producto1.marcaid._id;
        //setProductos([...productos.map(prod => prod.id === producto.id ? producto : prod)])
        modificarProducto(producto1);
    } 
    return (
        <Fragment>
        <tr>
            <td>{ producto1.marcaid.marca }</td>
            <td>{ producto1.modelo }</td>
            <td>{ producto1.precio }</td>
            <td>{ producto1.stock }</td>
            <td>
                {producto1.disponible === true ? 
                    <Button 
                        variant="success" 
                        className="badge"
                        onClick={() => onClickDisponible()}
                    >Disponible</Button>
                    : 
                    <Button 
                        variant="danger"
                        className="badge"
                        onClick={() => onClickDisponible()}
                    >Sin Stock</Button>
                }            
            </td>
            <td><Button variant="warning" className="badge" onClick={handleShow}>
            Ver +
            </Button>
            </td>
            <td>
                <ButtonGroup size="sm" className="ml-2">
                    <Button 
                        variant="secondary"
                        onClick={() => FormModificarProducto(producto1)}
                    >Modificar</Button>
                    <Button 
                        variant="dark" 
                        onClick={onClickEliminar}
                    >Borrar</Button>
                </ButtonGroup>
            </td>
        </tr>
        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Detalles</Modal.Title>
            </Modal.Header>
            <Modal.Body> 
                <ListarImg
                            cadaProducto={producto1}
                            marcas={marcas}
                />
            </Modal.Body>
        </Modal>
        </Fragment>
    )
}

export default Listaproductos;
