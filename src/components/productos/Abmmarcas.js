import React, { Fragment, useState, useEffect } from "react";
import { Row, Form, Button, Col, Table, Pagination } from "react-bootstrap";
import Listamarcas from "../productos/Listamarcas";

function Abmmarcas({ marcas, setMarcas, errores, setErrores, page }) {
  //state de item por paginas
  const [items, setItems] = useState([]);
  const [marca1, setMarca1] = useState({ marca: "" });
  const { marca } = marca1;

  const cambiarPage = async (numPage) => {
    const solicitud = await fetch(`${process.env.REACT_APP_BACKEND_URL}/api/marcas?porpagina=` + page.porpagina + '&pagina=' + numPage, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'x-auth-token': localStorage.getItem('token')
      }
    });
    const respuesta = await solicitud.json();
    if (solicitud.ok) {
      setMarcas(respuesta.marcas);
    } else {
      alert(respuesta.msg);
      return;
    }

    //armo la pagination
    const items1 = [];
    let active = marcas.page;
    for (let number = 1; number <= marcas.totalPages; number++) {
      items1.push(
        <Pagination.Item key={number} onClick={() => cambiarPage(number)} active={number === active}>
          {number}
        </Pagination.Item>,
      );
    }
    setItems([items1]);
  };

  // Cuando cambian los campos
  const onChangeMarca = (e) => {
    setMarca1({
      ...marca1,
      [e.target.name]: e.target.value,
    });
  };
  //funcion para crear la nueva marca
  const agregarMarca = async () => {
    const solicitud = await fetch(`${process.env.REACT_APP_BACKEND_URL}/api/marcas`, {
      method: "POST",
      body: JSON.stringify(marca1),
      headers: {
        "Content-Type": "application/json",
        "x-auth-token": localStorage.getItem("token"),
      },
    });
    const respuesta = await solicitud.json();
    if (solicitud.ok) {
      //setMarcas(respuesta.nuevamarca);
      const { docs, ...resto } = marcas;
      docs.push(respuesta.nuevamarca);
      resto.docs = docs;
      //setMarcas([...marcas, respuesta.nuevamarca]);
      setMarcas(resto);
      cambiarPage(marcas.page);
      //creo el mensaje de craeado
      //setErrores('La Marca fue Creada.');
      // Reiniciar formulario
      setMarca1({
        marca: "",
      });
    } else {
      //setErrores(respuesta.msg);
    }
    alert(respuesta.msg);
  };
  //funcion para modificar una marca
  const modificarMarca = async () => {
    const solicitud = await fetch(
      `${process.env.REACT_APP_BACKEND_URL}/api/marcas/` + marca1._id,
      {
        method: "PUT",
        body: JSON.stringify(marca1),
        headers: {
          "Content-Type": "application/json",
          "x-auth-token": localStorage.getItem("token"),
        },
      }
    );
    const respuesta = await solicitud.json();
    if (solicitud.ok) {
      //setMarcas(respuesta.nuevamarca);
      const { docs, ...resto } = marcas;
      const docs1 = docs.map((p) => (p._id === marca1._id ? marca1 : p));
      resto.docs = docs1;
      //setMarcas([...marcas.docs.map((p) => (p._id === marca1._id ? marca1 : p))]);
      setMarcas(resto);
      setMarca1({
        marca: "",
      });
    } else {
      //setErrores(respuesta.msg);
    }
    alert(respuesta.msg);
  };

  // agrego una marca
  const onSubmitNuevaMarca = (e) => {
    e.preventDefault();
    // Validaciones
    /*         if(marca1.marca.trim() === ''){
            return;
        } */

    if (!marca1.hasOwnProperty("_id")) {
      // Agregar al state
      /*             marca1.id = uuidv4();
            setMarcas([...marcas, marca1]); */
      // Agregar nueva marca
      agregarMarca();
    } else {
      // Modificacion
      //setMarcas([...marcas.filter(p => p.id !== marca1.id), marca1]);
      modificarMarca();
    }
    // Reiniciar formulario
    /*         setMarca1({
            marca: ''
        }); */
  }
  const ModificarMarca = (modi) => {
    //modi.usuario = modi.usuario._id;
    setMarca1(modi);
  }
  useEffect(() => {
    const pageTotal = () => {
      const items1 = [];
      let active = marcas.page;
      for (let number = 1; number <= marcas.totalPages; number++) {
        items1.push(
          <Pagination.Item key={number} onClick={() => cambiarPage(number)} active={number === active}>
            {number}
          </Pagination.Item>,
        );
      }
      setItems([items1]);
    }
    pageTotal();
    // eslint-disable-next-line
  }, [marcas]);

  return (
    <Fragment>
      <Row className="d-flex justify-content-center mt-5">
        <Col sm={4} className="p-5">
          <Form className="mb-5" onSubmit={onSubmitNuevaMarca}>
            <Form.Group controlId="formCategoriaNombre">
              <Form.Control
                type="text"
                name="marca"
                required
                placeholder="Ingrese la Marca"
                onChange={onChangeMarca}
                value={marca}
              />
            </Form.Group>
            <Button variant="primary" type="submit" block>
              {marca1.hasOwnProperty("_id")
                ? "Modificar Marca"
                : "Agregar Marca"}
            </Button>
          </Form>
        </Col>
        {/* </Row>
      <Row className="d-flex justify-content-center"> */}
        <Col className="">
          {
            marcas.hasOwnProperty("docs") ?
              <Table size="sm" responsive striped bordered hover>
                <thead>
                  <tr>
                    <th>Marca</th>
                    <th>Accion</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    marcas.docs.map((marca) => (
                      <Listamarcas
                        key={marca._id}
                        marca={marca}
                        marcas={marcas}
                        cambiarPage={cambiarPage}
                        setMarcas={setMarcas}
                        ModificarMarca={ModificarMarca}
                      />
                    ))
                  }
                </tbody>
              </Table>
              : null
          }
          <Row className="justify-content-center mt-3">
            <div>
              <Pagination>{items}</Pagination>
            </div>
          </Row>
        </Col>
      </Row>
    </Fragment>
  );
}

export default Abmmarcas;
