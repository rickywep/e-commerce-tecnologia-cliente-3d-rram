import React, {useState} from 'react'
import { Row, Form, Button, Col,  Table, Container } from 'react-bootstrap';
import Listaproveedor from '../productos/Listaproveedor'

function Abmproveedor({proveedores, setProveedores}) {
    // state del proveedor
    const [proveedor, setProveedor] = useState({
        nombre: "",
        email: "",
        celular: "",
        observaciones: ""
    });
    const {nombre, email, celular, observaciones} = proveedor;
    // Cuando cambian los campos
    const onChangeProveedor = e => {
        setProveedor({
            ...proveedor,
            [e.target.name]: e.target.value
        });
    };
    //funcion para crear el nuevo proveedor
    const agregarProveedor = async () => {
        const solicitud = await fetch(`${process.env.REACT_APP_BACKEND_URL}/api/proveedor`, {
            method: 'POST',
            body: JSON.stringify(proveedor),
            headers:{
                'Content-Type': 'application/json',
                'x-auth-token': localStorage.getItem('token')
            }
        });
        const respuesta = await solicitud.json();
        if (solicitud.ok) {
            setProveedores([...proveedores, respuesta.nuevoproveedor]);
            // Reiniciar formulario
            setProveedor({
                nombre: "",
                email: "",
                celular: "",
                observaciones: ""
            });
        }
        alert(respuesta.msg);
    }
    //funcion para modificar un proveedor
    const modiProveedor = async () => {
        const solicitud = await fetch(`${process.env.REACT_APP_BACKEND_URL}/api/proveedor/` + proveedor._id, {
            method: 'PUT',
            body: JSON.stringify(proveedor),
            headers:{
                'Content-Type': 'application/json',
                'x-auth-token': localStorage.getItem('token')
            }
        });
        const respuesta = await solicitud.json();
        if (solicitud.ok) {
            setProveedores([...proveedores.map(p => p._id === proveedor._id ? proveedor : p)]);
            setProveedor({
                nombre: "",
                email: "",
                celular: "",
                observaciones: ""
            });
        }
        alert(respuesta.msg);
    }
    // agrego un proveedor
    const onSubmitNuevoproveedor = e => {
        e.preventDefault();
        // Validaciones
        if(proveedor.nombre.trim() === '' || proveedor.email.trim() === '' || proveedor.celular < 1 || proveedor.observaciones.trim() === ''){
            alert('Debe completar los Campos requeridos!');
            return;
        }
        if (!proveedor.hasOwnProperty('_id')){
            // Agregar nuevo proveedor
            agregarProveedor();
        } else{
            // Modificacion
            modiProveedor();
        }
    }
    const ModificarProveedor= (modi) =>{
        //modi.usuario = modi.usuario._id;
        setProveedor(modi);
    }
    return (
        <Container>
            <Row className="d-flex justify-content-center">                
                <Form className="w-100 mt-5 mb-5" onSubmit={onSubmitNuevoproveedor}>
                    <Row>
                        <Col sm={4}>
                            <Form.Group controlId="nombre">
                                <Form.Control 
                                    type="text"
                                    name="nombre"
                                    required
                                    maxLength="80"
                                    placeholder="Ingrese el Nombre el Proveedor"
                                    onChange={onChangeProveedor}
                                    value={nombre}
                                />
                                <Form.Text className="text-muted">
                                Ingrese el Nombre el Proveedor.
                                </Form.Text>
                            </Form.Group>
                        </Col>
                        <Col sm={4}>                    
                            <Form.Group controlId="email">
                                <Form.Control 
                                    type="email"
                                    name="email"
                                    required
                                    placeholder="Ingrese el email el Proveedor"
                                    onChange={onChangeProveedor}
                                    value={email}
                                />
                                <Form.Text className="text-muted">
                                Ingrese el email el Proveedor.
                                </Form.Text>
                            </Form.Group>
                        </Col>
                        <Col sm={4}>
                            <Form.Group controlId="celular">
                                <Form.Control 
                                    type="text"
                                    name="celular"
                                    required
                                    maxLength="80"
                                    placeholder="Ingrese el tel./cel. del Proveedor"
                                    onChange={onChangeProveedor}
                                    value={celular}
                                />
                                <Form.Text className="text-muted">
                                Ingrese el tel./cel. del Proveedor.
                                </Form.Text>
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={12}>
                            <Form.Group controlId="observaciones">
                                <Form.Control 
                                    as="textarea" 
                                    rows="3"
                                    name="observaciones"
                                    required
                                    placeholder="Ingrese las observaciones del proveedor"
                                    onChange={onChangeProveedor}
                                    value={observaciones}
                                />
                                <Form.Text className="text-muted">
                                Ingrese las observaciones del proveedor.
                                </Form.Text>
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row className="justify-content-center">
                        <Col sm={3}>
                            <Button 
                                variant="primary"
                                type="submit"
                                block
                            >{ proveedor.hasOwnProperty('_id') ? 'Modificar Proveedor' : 'Agregar Proveedor' }
                            </Button>
                        </Col>
                    </Row>
                </Form>
            </Row>
            <Row className="d-flex justify-content-center">
                <Col className="mt-3">
                    {
                        proveedores.length > 0 ?
                            <Table responsive striped bordered hover>
                                <thead>
                                    <tr>
                                    <th>Proveedor</th>
                                    <th>Email</th>
                                    <th>Tel./Cel.</th>
                                    <th>Observaciones</th>                                    
                                    <th>Accion</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                    proveedores.map( (proveedor) => (
                                        <Listaproveedor
                                            key={proveedor._id}
                                            proveedor={proveedor}
                                            proveedores={proveedores}
                                            setProveedores={setProveedores}
                                            ModificarProveedor={ModificarProveedor}
                                        />
                                    ))
                                    }
                                </tbody>
                            </Table>
                        : null
                    }
                </Col>
            </Row>
        </Container>
    )
}

export default Abmproveedor
