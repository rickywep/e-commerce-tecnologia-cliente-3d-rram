import React from 'react';
import { Button, ButtonGroup } from 'react-bootstrap';

function Listamarcas({marca, marcas, cambiarPage, setMarcas, ModificarMarca}) {
    // Elimina una categoría
    const eliminarMarca = async(id) => {
        const solicitud = await fetch(`${process.env.REACT_APP_BACKEND_URL}/api/marcas/` + id, {
            method: 'DELETE',
            headers:{
                'Content-Type': 'application/json',
                'x-auth-token': localStorage.getItem('token')
            }
        });
        const respuesta = await solicitud.json();
        if (solicitud.ok) {
            cambiarPage(marcas.page);
            //setMarcas([...marcas.filter(p => p._id !== marca._id)]);
        } else {
            //setErrores(respuesta.msg);
        }
        alert(respuesta.msg);
    }

    // Eliminar la marca
    const onClickEliminar = () => {
        if(window.confirm('¿Esta seguro que desea Eliminar la Marca?')){
            eliminarMarca(marca._id);
            //setMarcas([...marcas.filter(p => p._id !== marca._id)]);
        }        
    }
    return (
        <tr>
            <td>{ marca.marca }</td>
            <td style={{width:'10rem'}}>
                <ButtonGroup size="sm" className="ml-2">
                    <Button 
                        className="mx-1"
                        variant="success"
                        onClick={() => ModificarMarca(marca)}
                    >
                    <i className="fas fa-pen"></i></Button>
                    <Button 
                        className="mx-1"
                        variant="danger" 
                        onClick={onClickEliminar}
                    >
                    <i className="fas fa-trash"></i>
                    </Button>
                </ButtonGroup>
            </td>
        </tr>
    )
}

export default Listamarcas
