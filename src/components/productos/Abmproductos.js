import React, { useState, useRef, useEffect } from 'react'
import { Row, Form, Button, Col, Table, Pagination, Container } from 'react-bootstrap';
import Listaproductos from '../productos/Listaproductos'
// import ListarImg from '../productos/ListarImg'
// import { v4 as uuidv4 } from 'uuid';

function Abmproductos({ productos, setProductos, marcas, errores, setErrores, page, datospageprod, setDatospageprod }) {
    const [producto, setProducto] = useState({
        estadoCel: "Seleccionar Estado...",
        marcaid: "",
        modelo: "",
        precio: 100.00,
        disponible: true,
        caracteristicas: "",
        imgCel: "",
        cantidad: 0
    });
    //saco las propiedades
    const {
        // _id, estadoCel, marcaid , disponible, imgCel, cantidad, 
        modelo, precio, caracteristicas } = producto;
    let textInput = useRef(null);

    //state de item por paginas
    const [items, setItems] = useState([]);

    // Cuando cambian los campos
    const onChangeProducto = e => {
        setProducto({
            ...producto,
            [e.target.name]: e.target.value
        });
    };
    const cambiarPage = async (numPage) => {
        const solicitud = await fetch(`${process.env.REACT_APP_BACKEND_URL}/api/productos?porpagina=` + page.porpagina + '&pagina=' + numPage, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'x-auth-token': localStorage.getItem('token')
            }
        });
        const respuesta = await solicitud.json();
        if (solicitud.ok) {
            const { docs, ...resto } = respuesta.productos;
            setProductos(docs);
            setDatospageprod(resto);
            const items1 = [];
            let active = resto.page;
            for (let number = 1; number <= resto.totalPages; number++) {
                items1.push(
                    <Pagination.Item key={number} onClick={() => cambiarPage(number)} active={number === active}>
                        {number}
                    </Pagination.Item>,
                );
            }
            setItems([items1]);
        } else {
            setErrores(respuesta.msg);
        }

    };
    // cuando cambia el campo imagen
    const onChangeImg = async e => {
        if (e.target.files[0]) {
            if (e.target.files[0].size > 4194304) {
                // 5242880 = 5MB
                // 4194304 = 4MB
                e.target.value = null;
                alert('La imagen es demasiado grande.');
                setProducto({
                    ...producto,
                    imgCel: null
                });
                return;
            }
            let reader = new FileReader();
            reader.readAsDataURL(e.target.files[0]);
            reader.onloadend = () => {
                setProducto({
                    ...producto,
                    imgCel: reader.result
                });
            };
        } else {
            setProducto({
                ...producto,
                imgCel: null
            });
        }
    };
    //funcion para crear un producto
    const agregarProducto = async () => {
        const solicitud = await fetch(`${process.env.REACT_APP_BACKEND_URL}/api/productos`, {
            method: 'POST',
            body: JSON.stringify(producto),
            headers: {
                'Content-Type': 'application/json',
                'x-auth-token': localStorage.getItem('token')
            }
        });
        const respuesta = await solicitud.json();
        if (solicitud.ok) {
            setProductos([...productos, respuesta.nuevoproducto]);;
            //creo el mensaje de creado
            //setErrores('El Producto fue Creado.');
            // Reiniciar formulario
            setProducto({
                estadoCel: "Seleccionar Estado...",
                marcaid: "",
                modelo: "",
                precio: 100.00,
                disponible: true,
                caracteristicas: "",
                imgCel: "",
                cantidad: 0
            });
        } else {
            //setErrores(respuesta.msg);
        }
        alert(respuesta.msg);
    }
    //funcion para modificar un producto
    const modificarProducto = async (prodmodi) => {
        const solicitud = await fetch(`${process.env.REACT_APP_BACKEND_URL}/api/productos/` + prodmodi._id, {
            method: 'PUT',
            body: JSON.stringify(prodmodi),
            headers: {
                'Content-Type': 'application/json',
                'x-auth-token': localStorage.getItem('token')
            }
        });
        const respuesta = await solicitud.json();
        if (solicitud.ok) {
            setProductos([...productos.map(p => p._id === respuesta.updateproducto._id ? respuesta.updateproducto : p)]);
            //setErrores('El Producto fue Modificado.');
            // Reiniciar formulario
            setProducto({
                estadoCel: "Seleccionar Estado...",
                marcaid: "",
                modelo: "",
                precio: 100.00,
                disponible: true,
                caracteristicas: "",
                imgCel: "",
                cantidad: 0,
                maraux: ""
            });
        } else {
            //setErrores(respuesta.msg);
        }
        alert(respuesta.msg);
    }
    // agrego un producto
    const onSubmitNuevoproducto = e => {
        e.preventDefault();
        if(producto.estadoCel==='Seleccionar Estado...'){
            alert('Debes completar el campo Estado') 
        } else if(producto.marcaid ===''){
            alert('Debes completar el campo Marca') 
        } else if(producto.imgCel=== null || producto.imgCel===''){
            alert('Debes adjuntar una imagen del producto') 
        } else{
        producto.precio = Number(producto.precio);
        if (typeof producto.disponible === 'string') { producto.disponible = producto.disponible === "true" ? true : false; }
        if (!producto.hasOwnProperty('_id')) {
            agregarProducto(producto);
        } else {
            modificarProducto(producto);
        }
    }}
    const FormModificarProducto = (modi) => {
        //modi.marcaid = modi.marcaid._id;
        const { marcaid, ...resto } = modi;
        resto.marcaid = marcaid._id;
        //textInput.current.value = modi.marcaid._id;
        setProducto(resto);
        //alert(textInput.current.value);        
    }
    useEffect(() => {
        const pageTotal = () => {
            const items1 = [];
            let active = datospageprod.page;
            for (let number = 1; number <= datospageprod.totalPages; number++) {
                items1.push(
                    <Pagination.Item key={number} onClick={() => cambiarPage(number)} active={number === active}>
                        {number}
                    </Pagination.Item>,
                );
            }
            setItems([items1]);
        }
        pageTotal();
        // eslint-disable-next-line
    }, [productos]);

    return (
        <Container>
            <Row className="d-flex justify-content-center">
                <Form className="w-100 my-3" onSubmit={onSubmitNuevoproducto}>
                    <Row>
                        <Col sm={4}>
                            <Form.Group >
                                <Form.Control as="select" value={producto.estadoCel} name="estadoCel" onChange={onChangeProducto}>
                                    <option default>Seleccionar Estado...</option>
                                    <option key={1} value="Nuevo">Nuevo</option>
                                    <option key={2} value="Usado">Usado</option>
                                </Form.Control>
                                <Form.Text className="text-muted">
                                    Seleccione el Estado.
                                </Form.Text>
                            </Form.Group>
                        </Col>
                        <Col sm={4}>
                            <Form.Group >
                                <Form.Control as="select" value={producto.marcaid} name="marcaid" onChange={onChangeProducto} ref={textInput} >
                                    <option value=''>Seleccionar Marca...</option>
                                    {marcas.map((marca) => (
                                        <option key={marca._id} value={marca._id}>
                                            {marca.marca}
                                        </option>
                                    ))}
                                </Form.Control>
                                <Form.Text className="text-muted">
                                    Seleccione la Marca.
                                </Form.Text>
                            </Form.Group>
                        </Col>
                        <Col sm={4}>
                            <Form.Group >
                                <Form.Control
                                    type="text"
                                    name="modelo"
                                    required
                                    placeholder="Ingrese el Modelo"
                                    onChange={onChangeProducto}
                                    value={modelo}
                                />
                                <Form.Text className="text-muted">
                                    Ingrese el Modelo.
                                </Form.Text>
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={3}>
                            <Form.Group >
                                <Form.Control
                                    type="number"
                                    name="precio"
                                    id="precio"
                                    min="1"
                                    placeholder="Precio"
                                    onChange={onChangeProducto}
                                    value={precio}
                                    required
                                />
                                <Form.Text className="text-muted">
                                    Ingrese el Precio de Venta.
                                </Form.Text>
                            </Form.Group>
                        </Col>
                        <Col sm={3}>
                            <Form.Group >
                                <Form.Control as="select" value={producto.disponible} name="disponible" onChange={onChangeProducto}>
                                    <option default>Seleccionar Disponible...</option>
                                    <option key={1} value={true} >Si</option>
                                    <option key={2} value={false} >No</option>
                                </Form.Control>
                                <Form.Text className="text-muted">
                                    Seleccione si esta Disponible.
                                </Form.Text>
                            </Form.Group>
                        </Col>
                        <Col sm={6}>
                            <Form.Group >
                                <Form.File
                                    id="imgCel"
                                    accept="image/*"
                                    onChange={onChangeImg}
                                />
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={12}>
                            <Form.Group >
                                <Form.Control
                                    as="textarea"
                                    rows="3"
                                    name="caracteristicas"
                                    required
                                    placeholder="Ingrese la Caracteristicas"
                                    onChange={onChangeProducto}
                                    value={caracteristicas}
                                />
                                <Form.Text className="text-muted">
                                    Ingrese las Caracteristicas.
                                </Form.Text>
                            </Form.Group>
                        </Col>
                    </Row>
                    <Button
                        variant="primary"
                        type="submit"
                        block
                    >{producto.hasOwnProperty('_id') ? 'Modificar Producto' : 'Agregar Producto'}
                    </Button>
                </Form>
            </Row>
            <Row className="d-flex justify-content-center">
                <Col className="mt-3">
                    {
                        productos.length > 0 ?
                            <Table responsive striped bordered hover>
                                <thead>
                                    <tr>
                                        <th>Marca</th>
                                        <th>Modelo</th>
                                        <th>Precio</th>
                                        <th>Stock</th>
                                        <th>Disponible</th>
                                        <th>Imagen</th>
                                        <th>Accion</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        productos.map((producto1, index) => (
                                            <Listaproductos
                                                key={index}
                                                producto1={producto1}
                                                productos={productos}
                                                setProductos={setProductos}
                                                FormModificarProducto={FormModificarProducto}
                                                marcas={marcas}
                                                index={index}
                                                errores={errores}
                                                setErrores={setErrores}
                                                modificarProducto={modificarProducto}
                                            />
                                        ))
                                    }
                                </tbody>
                            </Table>
                            : null
                    }
                </Col>
            </Row>
            <Row className="justify-content-center mt-3">
                <div>
                    <Pagination>{items}</Pagination>
                </div>
            </Row>
        </Container>
    )
}

export default Abmproductos
