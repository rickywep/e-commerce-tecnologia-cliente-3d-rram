import React, { useState, useContext } from "react";
import { Button, Modal, Form, Alert } from "react-bootstrap";
import clienteAxios from "../config/axios";
import AlertaContext from "../context/alertas/alertaContext";

const ModalCrearAdmin = ({ show, setShow }) => {

  const alertaContext = useContext(AlertaContext);
  const { alerta, mostrarAlerta } = alertaContext;

  const handleClose = () => {
    setUsuario({
      password: '',
      passwordconfirm: ''
    })
    setShow(false)
  };
  const [usuario, setUsuario] = useState({ password: "", passwordconfirm: "" });
  const { password, passwordconfirm } = usuario;
  
  const onChangeUsuario = (e) => {
    setUsuario({
      ...usuario,
      [e.target.name]: e.target.value,
      is_admin: true,
    });
  };
  // Cuando el usuario quiere registrarse
  const onSubmitRegistro = async (e) => {
    e.preventDefault();

    //password de 6 caracteres
    if (password.length < 6) {
      mostrarAlerta("El password debe ser al menos de 6 caracteres", "danger");
      return;
    }

    if (password !== passwordconfirm) {
      mostrarAlerta("Los passwords no son iguales", "danger");
      return;
    }

    try {
      await clienteAxios.post("/api/usuarios", usuario);
      handleClose()
      
    } catch (error) {
      mostrarAlerta(error.response.data.errores[0].msg, "danger");
    }
    
    
  };
  return (
    <Modal show={show} onHide={handleClose}>
      <Form onSubmit={onSubmitRegistro}>
        <Modal.Header closeButton style={{ backgroundColor: "var(--details)" }}>
          <Modal.Title className="text-white">Nuevo Admin</Modal.Title>
        </Modal.Header>
        <Modal.Body className="p-5">
        {alerta ? (
            <Alert variant={alerta.categoria} className="mt-2 text-center">
              {alerta.msg}
            </Alert>
          ) : null}
          <Form.Group controlId="formRegistroNombre">
            <Form.Control
              type="text"
              name="nombre"
              required
              placeholder="Nombre"
              onChange={onChangeUsuario}
            />
          </Form.Group>
          <Form.Group controlId="formRegistroEmail">
            <Form.Control
              type="email"
              name="email"
              required
              placeholder="Email"
              onChange={onChangeUsuario}
            />
          </Form.Group>
          <Form.Group controlId="formRegistroPassword">
            <Form.Control
              type="password"
              name="password"
              value={password}
              required
              placeholder="Password"
              onChange={onChangeUsuario}
            />
          </Form.Group>
          <Form.Group controlId="formRegistroPasswordconfirm">
            <Form.Control
              type="password"
              name="passwordconfirm"
              value={passwordconfirm}
              required
              placeholder="Confirmar password"
              onChange={onChangeUsuario}
            />
          </Form.Group>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cancelar
          </Button>
          <Button variant="primary" type="submit">
            Crear
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
};

export default ModalCrearAdmin;
