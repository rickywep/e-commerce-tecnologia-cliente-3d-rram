import React from "react";
import moment from 'moment'

const TurnoAdmin = ({ turno }) => {
  const { diaYHora, celular, nombre, email } = turno;
  let date = moment.utc(diaYHora).subtract(3, 'hours').format('DD/MM/YYYY hh:mm A')
  // let date = moment(new Date(date)).subtract(3, 'hours').format('DD/MM/YYYY hh:mm:ss A');

  return (       
          <tr>
            <td>{date}</td>
            <td>{nombre}</td>
            <td>{email}</td>
            <td>{celular}</td>
          </tr>
  );
};

export default TurnoAdmin;
