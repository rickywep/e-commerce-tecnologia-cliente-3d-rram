import React from "react";
import { Row, Col } from "react-bootstrap";
import { useLocation} from "react-router-dom";
import { HashLink as Link } from 'react-router-hash-link';

import LogoChico from "../images/logos/logo_chico.svg";
const Footer = () => {
  const location = useLocation()
  const {pathname} = location
  if (pathname.includes('/admin')|| pathname.includes('/login') || pathname.includes('/registro')) return null;
  return (
    <footer style={{ backgroundColor: "var(--details)" }} className="mt-5 container-fluid">
      <Row>
        <Col md={2}>
          <div className="rounded-circle m-2 pt-1" style={{backgroundColor:"white", width:"5rem", height:"5rem"}}>
            <Link to="/" className="nav-link">
              <img src={LogoChico} alt="logo grande" width="50" className="" />
            </Link>
          </div>
        </Col>
        <Col md={3} className="m-2 p-2">
          <div>
            <h6>Secciones</h6>
            <hr />
            <Link to="/#home" className="nav-link text-white">
              Inicio
            </Link>
            <Link to="/about#about" className="nav-link text-white">
              Sobre Nosotros
            </Link>
            <Link to="/servicios#servicios" className="nav-link text-white">
              Servicios
            </Link>
            <Link to="/tienda#tienda" className="nav-link text-white">
              Tienda
            </Link>
            <Link to="/turnos#turnos" className="nav-link text-white">
              Turnos
            </Link>
          </div>
        </Col>
        <Col md={3} className="m-2 p-2">
          <h6>Seguinos en nuestras redes</h6>
          <hr />
          <div
            className="text-white d-flex justify-content-around"
            style={{ fontSize: "3rem" }}
          >
            <i className="fab fa-facebook-square"></i>
            <i className="fab fa-twitter-square"></i>
            <i className="fab fa-instagram-square"></i>
            <i className="fab fa-youtube-square"></i>
          </div>
        </Col>
        <Col md={3} className="m-2 p-2 ">
          <h6>Contactanos</h6>
          <hr />
          <p className="text-white">Direccion: Calle falsa 123</p>
          <p className="text-white">San miguel de Tucuman</p>
          <p className="text-white">Tucuman</p>
          <p className="text-white">Tel: 3816768765</p>
        </Col>
      </Row>
    </footer>
  );
};

export default Footer;
