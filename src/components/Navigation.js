import React, { useContext, useEffect } from "react";
import { Navbar, Nav, Button, Badge, Dropdown } from "react-bootstrap";
import { Link, useLocation, NavLink } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faShoppingCart } from "@fortawesome/free-solid-svg-icons";
import LogoGrande from "../images/logos/logo_grande.svg";
import LogoChico from "../images/logos/logo_chico.svg";
import AuthContext from "../context/auth/authContext";

const Navigation = ({ productosCarrito }) => {

  const authContext = useContext(AuthContext);
  const { usuario, usuarioAutenticado, cerrarSesion } = authContext;
  const location = useLocation()
  
  useEffect(() => {
    usuarioAutenticado();
    // eslint-disable-next-line
  }, []);

  const onClickCerrarSesion = () => {
    let c = window.confirm("¿esta seguro que quiere cerrar sesion?");
    if (c === true) {
      cerrarSesion();
      window.location.reload();
    }
  };
  const { pathname } = location
  if (pathname.includes('/admin') || pathname.includes('/login') || pathname.includes('/registro')) return null;
  return (
    <Navbar
      expand="lg"
      variant="dark"
      style={{ backgroundColor: "var(--background)" }}
    >
      <Navbar.Brand>
        <Link as={NavLink} to="/" className="nav-link ">
          <img src={LogoGrande} alt="logo grande" className="d-none d-sm-none d-md-none d-lg-block" />
          <img src={LogoChico} alt="logo grande" width="40" className="d-block d-sm-block d-md-block d-lg-none" />
        </Link>
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link as={NavLink} exact to="/" className="nav-link">
            Inicio
          </Nav.Link>
          <Nav.Link as={NavLink} to="/about" className="nav-link">
            Nosotros
          </Nav.Link>
          <Nav.Link as={NavLink} to="/servicios" className="nav-link">
            Servicios
          </Nav.Link>
          <Nav.Link as={NavLink} to="/tienda" className="nav-link">
            Tienda
          </Nav.Link>
          <Nav.Link as={NavLink} to="/turnos" className="nav-link">
            Turnos
          </Nav.Link>
        </Nav>
        <Navbar.Brand>
          {usuario ? (
            <Navbar.Text className="mr-1 text-white" style={{ fontSize: '1rem' }}>
              <Dropdown>
                <Dropdown.Toggle style={{ backgroundColor: 'var(--background)', borderColor: 'var(--background)' }} id="dropdown-basic">
                  <span className="text-uppercase">{usuario.nombre} </span>
                  <i style={{ color: 'var(--details)' }} className="fas fa-user"></i>
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item className="text-dark" as={Link} to="/miscompras">Mis Compras</Dropdown.Item>
                  <Dropdown.Divider />
                  <Dropdown.Item className="text-dark" onClick={onClickCerrarSesion} >Cerrar sesion</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </Navbar.Text>
          ) : (
              <Button
                variant="outline-dark"
                as={Link}
                to="/login"
                className="mr-1 text-white"
                style={{ backgroundColor: "var(--details)" }}
              >
                Ingresar
              </Button>
            )}
        </Navbar.Brand>
        <Button
          variant="outline-dark"
          className="text-white"
          as={Link}
          to="/carrito"
          style={{ backgroundColor: "var(--details)" }}
        >
          <FontAwesomeIcon icon={faShoppingCart} className="mr-1" />
          <Badge variant="info">
            {productosCarrito && productosCarrito.length}
          </Badge>
        </Button>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default Navigation;
