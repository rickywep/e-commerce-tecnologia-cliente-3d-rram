import React, { useState, useContext } from "react";
import { Container, Row, Col, Form, Button, Alert } from "react-bootstrap";
import clienteAxios from "../config/axios";
import AlertaContext from "../context/alertas/alertaContext";

const Contact = () => {
  const alertaContext = useContext(AlertaContext);
  const { alerta, mostrarAlerta } = alertaContext;

  const [mensajes, setMensajes] = useState({
    nombre: "",
    apellido: "",
    email: "",
    celular: "",
    contenido: "",
  });

  const [cel, setCel] = useState('')

  const { nombre, apellido, email, celular, contenido } = mensajes;
  const onChangeCel = (e) => {
    const re = /^[0-9\b]+$/;
    // if value is not blank, then test the regex
    if (e.target.value === '' || re.test(e.target.value)) {
      setCel(e.target.value)
      setMensajes({
        ...mensajes,
        celular: e.target.value,
      })
    }
  };
  // Rellenar los campos mientras se tipea
  const onChangeMensaje = (e) => {
    setMensajes({
      ...mensajes,
      [e.target.name]: e.target.value,
    });
  };
  const onSubmitMensaje = async (e) => {
    e.preventDefault();

    setMensajes({
      nombre,
      apellido,
      email,
      celular,
      contenido,
    });
    try {

      await clienteAxios.post("/api/mensajes", mensajes);
      setMensajes({
        nombre: '',
        apellido: '',
        email: '',
        celular: '',
        contenido: ''
      });
      setCel('')
      mostrarAlerta("Mensaje Enviado Correctamente", "success");
    } catch (error) {
      mostrarAlerta(error.response.data.errores[0].msg, "danger");
    }


  };
  return (
    <Row className="my-5">
      <Col sm={12} lg={6} className="mt-5">
        <p className="mt-5">{""} </p>
        <h1 className="text-center mt-5" style={{ color: "var(--details)" }}>
          Contactate con nosotros
        </h1>
        <p className="text-center m-3">
          Dejanos tus datos y cualquier consulta que tengas. Te respondremos lo
          antes posible
        </p>
      </Col>
      <Col sm={12} lg={6}>
        <Container className="mt-5">
          {alerta ? (
            <Alert variant={alerta.categoria} className="mt-2 text-center">
              {alerta.msg}
            </Alert>
          ) : null}
          <Form onSubmit={onSubmitMensaje}>
            <Row>
              <Col>
                <Form.Control
                  placeholder="Nombre"
                  type="text"
                  name="nombre"
                  value={nombre}
                  required
                  maxLength="30"
                  onChange={onChangeMensaje}
                />
              </Col>
              <Col>
                <Form.Control
                  placeholder="Apellido"
                  type="text"
                  name="apellido"
                  value={apellido}
                  required
                  maxLength="30"
                  onChange={onChangeMensaje}
                />
              </Col>
            </Row>
            <Row className="mt-4">
              <Col>
                <Form.Control
                  placeholder="Email"
                  type="email"
                  name="email"
                  value={email}
                  maxLength="50"
                  required
                  onChange={onChangeMensaje}
                />
              </Col>
              <Col>
                <Form.Control
                  placeholder="Celular"
                  value={cel}
                  maxLength="12"
                  type="text"
                  name="celular"
                  required
                  onChange={onChangeCel}
                />
              </Col>
            </Row>
            <Row className="mt-4">
              <Col>
                <Form.Control
                  as="textarea"
                  rows="3"
                  placeholder="Escribe tu mensaje..."
                  type="text"
                  name="contenido"
                  maxLength="150"
                  value={contenido}
                  required
                  onChange={onChangeMensaje}
                />
              </Col>
            </Row>
            <Button
              className="mt-4 text-white float-right"
              variant="dark"
              type="submit"
              style={{ backgroundColor: "var(--details)" }}
            >
              Enviar
            </Button>
          </Form>
        </Container>
      </Col>
    </Row>
  );
};

export default Contact;
