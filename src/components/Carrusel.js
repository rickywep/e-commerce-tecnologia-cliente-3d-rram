import React from "react";
import { Carousel } from "react-bootstrap";
import ImageCarousel1 from "../images/carrusel_4.png";
import ImageCarousel2 from "../images/carrusel_5.png";
import ImageCarousel3 from "../images/carrusel_6.png";

const Carrusel = () => {
  return (
    <Carousel className="carrusel">
      <Carousel.Item>
        <img className="d-block img-fluid" src={ImageCarousel1} style={{maxHeight:'300px'}} alt="First slide" />
        <Carousel.Caption className="mb-5">
          {/* <div className="mb-5 caption-carrusel">
            <h3>First slide label</h3>
            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
          </div> */}
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img className="d-block img-fluid" src={ImageCarousel2} style={{maxHeight:'300px'}} alt="Third slide" />

        <Carousel.Caption className="mb-5">
          {/* <div className="mb-5 caption-carrusel">
            <h3>Second slide label</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          </div> */}
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img className="d-block img-fluid" src={ImageCarousel3} style={{maxHeight:'300px'}} alt="Third slide" />

        <Carousel.Caption className="mb-5">
          {/* <div className="mb-5 caption-carrusel"> */}
          {/* <h3>Third slide label</h3>
            <p>
              Praesent commodo cursus magna, vel scelerisque nisl consectetur.
            </p>
          </div> */}
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
};

export default Carrusel;
