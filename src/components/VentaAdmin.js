import React from 'react';

const VentaAdmin = ({ venta }) => {
    const { nombre, apellido, provincia, ciudad, domicilio, telefono, productosComprados } = venta
    return (
        <tr>
            <td >
                {productosComprados.map((pc,i) => (
                    <div key={i}>
                        {pc.modelo}
                        ({pc.cantidad})
                        ${pc.precio}
                        <br />
                    </div>
                ))}
            </td>
            <td>{nombre}<br />{apellido}</td>
            <td>{provincia}<br />{ciudad}</td>
            <td>{domicilio}</td>
            <td>{telefono}</td>
        </tr>
    );
};

export default VentaAdmin;