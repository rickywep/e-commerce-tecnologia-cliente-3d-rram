import React from 'react'
import { CompraUsuario } from './CompraUsuario'

export const ComprasUsuario = ({compras}) => {
    return (
        <div>
             {compras.map((compra) => (
              (<CompraUsuario key={compra._id} compra={compra} />)
            
          ))}
        </div>
    )
}
