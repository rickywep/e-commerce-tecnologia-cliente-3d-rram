import React from "react";
const UsuarioAdmin = ({ usuario }) => {
  const { nombre, email } = usuario;

  return (
    <tr>
      <td>{nombre}</td>
      <td>{email}</td>
    </tr>
  );
};

export default UsuarioAdmin;
