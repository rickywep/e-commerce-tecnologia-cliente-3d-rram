import React, { useContext, useEffect } from "react";
import LogoGrande from "../images/logos/logo_grande.svg";
import { useLocation, NavLink } from "react-router-dom";
import { Nav, Navbar, Button } from "react-bootstrap";
import AuthContext from "../context/auth/authContext";
const NavigationAdmin = () => {
  const authContext = useContext(AuthContext);
  const { usuario, usuarioAutenticado, cerrarSesion } = authContext;
  useEffect(() => {
    usuarioAutenticado();
    // eslint-disable-next-line
  }, []);
  const location = useLocation();
  const { pathname } = location;
  if (!pathname.includes("/admin")) return null;


  return (
    <Navbar
      expand="lg"
      variant="dark"
      style={{ backgroundColor: "var(--background)" }}
    >
      <Navbar.Brand className="m-2">
        <img src={LogoGrande} alt="logo grande" />
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link as={NavLink} to="/admin/usuarios" className="nav-link">
            Usuarios
          </Nav.Link>
          <Nav.Link as={NavLink} to="/admin/celulares" className="nav-link">
            Celulares
          </Nav.Link>
          <Nav.Link as={NavLink} to="/admin/stock" className="nav-link">
            Stock
          </Nav.Link>
          <Nav.Link as={NavLink} to="/admin/turnos" className="nav-link">
            Turnos
          </Nav.Link>
          <Nav.Link as={NavLink} to="/admin/mensajes" className="nav-link">
            Mensajes
          </Nav.Link>
          <Nav.Link as={NavLink} to="/admin/ventas" className="nav-link">
            Ventas
          </Nav.Link>
        </Nav>
        {usuario && (
          <Navbar.Brand>
            <Navbar.Text className="mr-2 text-white">
              {usuario.nombre}
            </Navbar.Text>
            <Button
              variant="outline-dark"
              size="sm"
              onClick={() => cerrarSesion()}
              href="/login"
              className="mr-1 mb-1 text-white"
            >
              Cerrar Sesión
          </Button>
          </Navbar.Brand>
        )}
      </Navbar.Collapse>
    </Navbar>
  );
};

export default NavigationAdmin;
