import React, { useEffect } from "react";
import { Col, Button, Row, Badge } from "react-bootstrap";
import moment from "moment";

function TurnosDisponibles({
  turnos,
  turno,
  dia,
  setTurnoElegido,
  turnoElegido,
  crearTurno,
  setNuevo,
  nuevo,
}) {
  useEffect(() => {
    setNuevo({
      ...nuevo,
      diaYHora: turnoElegido,
      dia: moment(new Date (dia)).format("YYYY-MM-DD"),
      // dia
    });
    // eslint-disable-next-line
  }, [turnoElegido]);

  const cambiarTurnoElegido = (t) => {
    setTurnoElegido(moment(new Date(t)).format());
    };
  const negarTurnoElegido = (t) => {
    setTurnoElegido("");
    alert("El turno elegido no esta disponible");
  };

  return (
    <>
      <Row className="">
        <Col>
          {turnos === null ? null : dia === "" ? null : (
            <div>
              <Badge className="mr-2" pill variant="secondary">
                No disponible
              </Badge>
              <Badge className="m-2" pill variant="info">
                Disponible
              </Badge>
            </div>
          )}
          <Col>
            <p>{turnos === null ? null : dia === "" ? null : "Mañana"}</p>
            <p>
              {turnos === null
                ? null
                : turno.map((t, index) => {
                    return moment(new Date(t)).format("hh") === 12 ? (
                      <Button 
                        key={index}
                        onClick={() => {
                          turnos.find(
                            (turno) =>
                              moment(new Date(turno.diaYHora)).format(
                                "YYYY-MM-DD hh:mm"
                              ) === moment(new Date(t)).format("YYYY-MM-DD hh:mm")
                          )
                            ? negarTurnoElegido(t)
                            : cambiarTurnoElegido(t);
                        }}
                        className={`m-1 ${moment(turnoElegido).format("LT") === moment(t).format("LT") ? 'active' : 'btn-sm'}`}
                        variant={
                          turnos.find(
                            (turno) =>
                              moment(new Date(turno.diaYHora)).format(
                                "YYYY-MM-DD hh:mm"
                              ) === moment(t).format("YYYY-MM-DD hh:mm")
                          )
                            ? "secondary" 
                            : "primary"

                        }
                        
                      >
                        {moment(t).format("LT")}
                      </Button>
                    ) : moment(t).format("a") === "am" ? (
                      <Button 
                        key={index}
                        onClick={() => {
                          turnos.find(
                            
                            (turno) =>
                              moment(turno.diaYHora).format(
                                "YYYY-MM-DD hh:mm"
                              ) === moment(t).format("YYYY-MM-DD hh:mm"))
                          
                            ? negarTurnoElegido(t)
                            : cambiarTurnoElegido(t)
                        }}
                        className={`m-1 ${moment(turnoElegido).format("LT") === moment(t).format("LT") ? 'active' : 'btn-sm'}`}
                        
                        variant={
                          turnos.find(
                            (turno) =>
                              moment(turno.diaYHora).format(
                                "YYYY-MM-DD h:mm"
                              ) === moment(t).format("YYYY-MM-DD h:mm")
                          )
                            ? "secondary" 
                            : "info"
                        }
                        disabled={ turnos.find(
                          (turno) =>
                            moment(turno.diaYHora).format(
                              "YYYY-MM-DD h:mm"
                            ) === moment(t).format("YYYY-MM-DD h:mm")
                        )
                          ? true
                          : false
                      }
                      >
                        {moment(t).format("LT")}
                      </Button>
                    ) : null;
                  })}
            </p>
          </Col>
          <Col md={9}>
            <p>{turnos === null ? null : dia === "" ? null : "Tarde"} </p>
            <p>
              {turnos === null
                ? null
                : turno.map((t, index) => {
                    return moment(t).format("hh") !== 12 ? (
                      moment(t).format("a") === "pm" ? (
                        <Button 
                          key={index}
                          onClick={() => {
                            turnos.find(
                              (turno) =>
                                moment(turno.diaYHora).format(
                                  "YYYY-MM-DD h:mm"
                                ) === moment(t).format("YYYY-MM-DD h:mm")
                            )
                              ? negarTurnoElegido(t)
                              : cambiarTurnoElegido(t);
                          }}
                          className={`m-1 ${moment(turnoElegido).format("LT") === moment(t).format("LT") ? 'active' : 'btn-sm'}`}

                          variant={
                            turnos.find(
                              (turno) =>
                                moment(turno.diaYHora).format(
                                  "YYYY-MM-DD h:mm"
                                ) === moment(t).format("YYYY-MM-DD h:mm")
                            )
                              ? "secondary"
                              : "info"
                          }
                          disabled={ turnos.find(
                            (turno) =>
                              moment(turno.diaYHora).format(
                                "YYYY-MM-DD h:mm"
                              ) === moment(t).format("YYYY-MM-DD h:mm")
                          )
                            ? true
                            : false
                        }
                        >
                          {moment(t).format("LT")}
                        </Button>
                      ) : null
                    ) : null;
                  })}
            </p>
          </Col>
          {dia === "" ? null : (
            <Button size="sm"
              variant="dark"
              onClick={crearTurno}
              type="submit"
              className="float-right mb-5"
              style={{ backgroundColor: "var(--details)" }}
            >
              Solicitar turno
            </Button>
          )}
        </Col>
      </Row>
    </>
  );
}

export default TurnosDisponibles;
