import React, { useState } from "react";
import { Container } from "react-bootstrap";
import FormularioTurnos from "./FormularioTurnos";
import SuccessTurno from "../../pages/user/SuccessTurno";

function Turnos() {
  const [datos, setDatos] = useState('')

  return (
      <Container className="mt-5" id="turnos">
        {datos.length === 0 ?
          <FormularioTurnos setDatos={setDatos} />
          :
          <SuccessTurno datos={datos} />
        }
      </Container>
  );
}

export default Turnos;
