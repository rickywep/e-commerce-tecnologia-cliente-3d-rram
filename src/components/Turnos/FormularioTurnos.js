import React, { useState, useEffect, useContext } from "react";
import { Form, Col, Row, Alert } from "react-bootstrap";
import Calendario from "react-calendar";
import TurnosDisponibles from "./TurnosDisponibles";
import "react-calendar/dist/Calendar.css";
import moment from "moment";
import clienteAxios from "../../config/axios";
import AlertaContext from "../../context/alertas/alertaContext";
import axios from "axios";

var tomorrow = new Date();
tomorrow.setDate(new Date().getDate() + 1);

function FormularioTurnos({ setDatos }) {

  const alertaContext = useContext(AlertaContext);
  const { alerta, mostrarAlerta } = alertaContext;

  const [dia, setDia] = useState("");
  const [turno, setTurno] = useState([]);
  const [turnoElegido, setTurnoElegido] = useState("");
  const [turnos, setTurnos] = useState(null);
  const [cel, setCel] = useState('')
  const [nuevo, setNuevo] = useState({
    nombre: "",
    email: "",
    celular: "",
    diaYHora: "",
    dia: "",
  });

  const { nombre, email, celular, diaYHora } = nuevo;

  const onChangeNuevo = (e) => {
    const re = /^[0-9\b]+$/;
    // if value is not blank, then test the regex
    if (e.target.value === '' || re.test(e.target.value)) {
      setCel(e.target.value)
    }
    setNuevo({
      ...nuevo,
      [e.target.name]: e.target.value,
    });
  };

  const onChange = (e) => {
    const turnosDia = [];
    setDia(moment(new Date(e)).format("l"));
    for (let hora = 9; hora < 21; hora++) {
      if (hora < 13 || hora > 16) {
        for (let minutos = 0; minutos < 59; minutos += 30) {
          const diaSeleccionado = new Date(e);
          diaSeleccionado.setHours(hora, minutos);
          turnosDia.push(diaSeleccionado);
        }
      }
    }
    setTurno(turnosDia);
  };

  const crearTurno = async (e) => {
    e.preventDefault();
    if (
      nombre.trim() === "" ||
      email.trim() === "" ||
      celular.trim() === "" ||
      diaYHora.trim() === "" ||
      dia.trim() === ""
    ) {
      mostrarAlerta("Completa tus datos y selecciona dia y hora", "danger");
      return;
    }
    try {
      await clienteAxios.post("/api/turnos", nuevo);
      setDatos(nuevo)
    } catch (error) {
      error.response.data.errors.map((err) => (
        mostrarAlerta(err.msg, "danger")
      ))
    }
  };

  async function fetchData() {
    const respuesta = await axios({
      url: `${process.env.REACT_APP_BACKEND_URL}/api/turnos/${moment(new Date(dia)).format(
        "YYYY-MM-DD"
      )}`,
    });
    setTurnos(respuesta.data.turnos);
  }

  useEffect(() => {
    fetchData();
    // eslint-disable-next-line
  }, [dia]);

  return (
    <Form>
      <Row>
        <Col lg={6}>
          <Form.Group controlId="formNombre">
            <Form.Control
              type="text"
              placeholder="Nombre"
              name="nombre"
              maxLength="30"
              required
              onChange={onChangeNuevo}
            />
          </Form.Group>
          <Form.Group controlId="formEmail">
            <Form.Control
              type="text"
              placeholder="Email"
              name="email"
              maxLength="30"
              required
              onChange={onChangeNuevo}
            />
          </Form.Group>
          <Form.Group controlId="formCelular">
            <Form.Control
              type="cellphone"
              placeholder="Celular"
              value={cel}
              maxLength="12"
              name="celular"
              required
              onChange={onChangeNuevo}
            />
          </Form.Group>
        </Col>
        <Col lg={6}>
          <h4 className="text-center my-5" style={{ color: "var(--details)" }}>
            Dejanos tus datos, selecciona el dia y horario para venir a nuestro
            local.
          </h4>
        </Col>
      </Row>
      {alerta ? (
        <Alert variant={alerta.categoria} className="mt-2 text-center">
          {alerta.msg}
        </Alert>
      ) : null}
      <Row className="my-5">
        <Col xs={12} md={6}>
          <h5 style={{ color: "var(--details)" }}>
            Selecciona el dia y horario
          </h5>

          <Calendario onChange={onChange} minDate={tomorrow} />
        </Col>
        <Col xs={12} md={6}>
          {moment(new Date(dia)).format("dddd") !== "Sunday" ? (
            <TurnosDisponibles
              turno={turno}
              turnos={turnos}
              setTurnoElegido={setTurnoElegido}
              turnoElegido={turnoElegido}
              dia={dia}
              crearTurno={crearTurno}
              nuevo={nuevo}
              setNuevo={setNuevo}
            />
          ) : null}
        </Col>
      </Row>
    </Form>
  );
}

export default FormularioTurnos;
