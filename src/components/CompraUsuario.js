import React from 'react'
import { Button, Accordion, Container, Table } from 'react-bootstrap';
import moment from 'moment'


export const CompraUsuario = ({ compra }) => {
    const { fecha, nombre, apellido, provincia, ciudad, domicilio, productosComprados, telefono } = compra
    let date = moment.utc(fecha).subtract(3, 'hours').format('DD/MM/YYYY')
    return (
        <div className="mt-5">
            <Accordion defaultActiveKey="1">
                <Accordion.Toggle as={Button} variant="link" eventKey="0" className="text-dark font-weight-bold" >
                    Compra realizada el: {date}<i className="fas fa-caret-down ml-2" style={{ color: 'var(--details)' }}></i>
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="0">
                    <Container className="p-5 table-responsive">
                        <Table bordered>
                            <tbody>
                                <tr>
                                    <th>A nombre de </th>
                                    <th>Provincia</th>
                                    <th>Ciudad</th>
                                    <th>Domicilio</th>
                                    <th>Telefono</th>
                                </tr>
                                <tr style={{ color: '#b2a6a6', fontSize: '0.8rem' }} >
                                    <th>{nombre}{" "}{apellido}</th>
                                    <th>{provincia}</th>
                                    <th>{ciudad}</th>
                                    <th>{domicilio}</th>
                                    <th>{telefono}</th>

                                </tr>
                            </tbody>
                        </Table>
                        <Table bordered>
                            <thead style={{ backgroundColor: "var(--details)", color: "white" }}>
                                <tr>
                                    <th>Productos</th>
                                    <th>cantidad</th>
                                    <th>modelo</th>
                                    <th>precio</th>
                                </tr>
                            </thead>
                            <tbody>
                                {productosComprados.map((pc, i) => (
                                    (<tr key={i} style={{ color: '#b2a6a6', fontSize: '0.8rem' }} >
                                        <th>
                                            <img alt="..." src={pc.imgCel} width="50" />
                                        </th>
                                        <th>{pc.cantidad}</th>
                                        <th>{pc.modelo}</th>
                                        <th>{pc.precio}</th>
                                    </tr>)
                                ))}
                            </tbody>
                        </Table>
                    </Container>
                </Accordion.Collapse>
            </Accordion>
        </div>
    )
}
