import React from 'react'
import { Switch, Route } from 'react-router-dom'
import MensajesAdmin from '../pages/admin/MensajesAdmin'
import TurnosAdmin from '../pages/admin/TurnosAdmin'
import Stock from '../components/productos/Stock'
import VentasAdmin from '../pages/admin/VentasAdmin'
import About from '../pages/user/About'
import Login from '../pages/auth/Login'
import SuccessCompra from '../pages/user/SuccessCompra'
import { MisCompras } from '../pages/user/MisCompras'
import Registro from '../pages/auth/Registro'
import UsuariosAdmin from '../pages/admin/UsuariosAdmin'
import Productos from '../components/productos/Productos'
import Servicios from '../pages/user/Servicios'
import Home from '../pages/user/Home'
import Carrito from '../components/Tienda/Carrito'
import Turnos from '../components/Turnos/Turnos'
import Tienda from '../components/Tienda/Tienda'

export const Routes = ({productosCarrito, setProductosCarrito}) => {

    return (

        <Switch>
            <Route path="/admin/mensajes" exact>
                <MensajesAdmin />
            </Route>
            <Route path="/admin/turnos" exact>
                <TurnosAdmin />
            </Route>
            <Route path="/admin/usuarios" exact>
                <UsuariosAdmin />
            </Route>
            <Route path="/admin/celulares" exact>
                <Productos />
            </Route>
            <Route path="/admin/stock" exact>
                <Stock />
            </Route>
            <Route path="/admin/ventas" exact>
                <VentasAdmin />
            </Route>
            <Route path="/about" exact>
                <About />
            </Route>
            <Route path="/servicios" exact>
                <Servicios />
            </Route>
            <Route exact path="/">
                <Home />
            </Route>
            <Route exact path="/login" component={Login} />
            <Route exact path="/registro" component={Registro} />
            <Route exact path="/success" component={SuccessCompra} />
            <Route exact path="/miscompras" component={MisCompras} />
            <Route exact path="/carrito">
                <Carrito
                    productosCarrito={productosCarrito}
                    setProductosCarrito={setProductosCarrito}
                />
            </Route>
            <Route exact path="/turnos">
                <Turnos />
            </Route>
            <Route path="/tienda" exact>
                <Tienda
                    setProductosCarrito={setProductosCarrito}
                    productosCarrito={productosCarrito}
                />
            </Route>
        </Switch>
    )
}
