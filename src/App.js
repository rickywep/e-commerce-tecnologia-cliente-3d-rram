import React, { useState } from "react";
import "./App.css";
import { BrowserRouter as Router } from "react-router-dom";
import AuthState from "./context/auth/authState";
import AlertaState from "./context/alertas/alertaState";
import tokenAuth from "./config/tokenAuth";
import Navigation from "./components/Navigation";
import Footer from "./components/Footer";
import "./ReactSpiner.css";
import NavigationAdmin from "./components/NavigationAdmin";
import { Routes } from "./routes/Routes";

//revisar si tenemos token
const token = localStorage.getItem("token");
if (token) {
  tokenAuth(token);
}
let compras = localStorage.getItem("compras");

function App() {

  const [productosCarrito, setProductosCarrito] = useState(JSON.parse(compras));

  return (
    <AuthState>
      <AlertaState>
        <Router>
          <Navigation productosCarrito={productosCarrito} />
          <NavigationAdmin />
          <Routes
            productosCarrito={productosCarrito}
            setProductosCarrito={setProductosCarrito}
          />
          <Footer />
        </Router>
      </AlertaState>
    </AuthState>
  );
}

export default App;
